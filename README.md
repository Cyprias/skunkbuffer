# SkunkBuffer   

A [SkunkWorks](http://skunkworks.sourceforge.net) buff bot script for Asheron's Call emulator server.

## Functionality
- Buff players on request.
- Split buff queue to other fellowship members.
- Summon portals on request.
- Respond to comps command.

## Installation
- Install Decal. [decaldev.com](https://www.decaldev.com)
- Download SkunkWorks for Asheron's Call. [SkunkWorks35-500.exe](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks35-500.exe/download)  
- Install SkunkWorks to `C:/Games/Skunkworks`. Some users experience problems running SkunkWorks in the default Program Files directory.
- Download updated SkunkWorks skapi.dll file. [SkunkWorks3.5.509.zip](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks3.5.509.zip/download)
- Extract SkunkWorks3.5.509.zip to your SkunkWorks directory, overwriting the existing skapi.dll file.
- Download [SkunkBuffer](https://gitlab.com/Cyprias/SkunkBuffer/-/releases) and extract it to your SkunkWorks directory. 
- Launch AC and run the `SkunkBuffer.swx` file from the in game Skunkworks UI.


## Updating SkunkBuffer
- Download a newer version from [here](https://gitlab.com/Cyprias/SkunkBuffer/-/releases)  
- Open the zip and extract the files over your existing SkunkBuffer folder, overwriting the previous files. Your settings will remain intact.  

## Infrequently Asked Questions
##### Is this ready for production?
Not really. 

##### How do I run SkunkBuffer automatically when logging in?  
Run SkunkBuffer so it shows up in the in game dropdown menu.
Open ThwargLauncher, go into the `Advanced View`, click the `OnLogin Cmds` button, select your bot character(s) and input `/swc run SkunkBuffer.swx` into the text area and click Save.  
Launch AC and see if SkunkBuffer runs upon login.

##### I'm getting a `>> Skunk***.js: File not found` error. What do I do?  
Simple answer: The zip file GitLab creates lack certain files. Goto the [releases](https://gitlab.com/Cyprias/SkunkBuffer/-/releases) page and use the `Download: SkunkBuffer_x.x.xxx.zip` link in the description.   
Technical answer: This repo uses [Git submodules](https://www.atlassian.com/git/tutorials/git-submodule) to share common files used in multiple SkunkWorks projects. GitLab doesn't include submodules files in their download link.  

##### I can't see Skunkworks' icon on the Decal bar.
Try installing the following libraries, then restart your PC.  
- [Microsoft .NET Framework 2.0 Service Pack 2](https://www.microsoft.com/en-us/download/details.aspx?id=1639)  
- [MSXML (Microsoft XML Parser) 3.0 Service Pack 4 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=23412)  
- [MSXML 4.0 Service Pack 3 (Microsoft XML Core Services)](https://www.microsoft.com/en-us/download/details.aspx?id=15697)  
- [Microsoft Core XML Services (MSXML) 6.0](https://www.microsoft.com/en-us/download/details.aspx?id=3988)  

Also try running ThwargleLauncher without admin privileges.


##### Why does SkunkWorks reduce my frame rate so much?  
SkunkWorks uses Decal's old style render engine which isn't as efficient as the newer [Virindi Views](http://www.virindi.net/wiki/index.php/Virindi_Views) render engine that many plugins use today.  
Minimizing script UI should alleviate the issue.


## External links
- [SkunkBuffer source repo](https://gitlab.com/Cyprias/SkunkBuffer)  
- [SkunkWorks Discord channel](https://discord.gg/z5wXR5K)  

## Contributors
- [Cyprias](https://gitlab.com/cyprias)

## License
MIT License	(http://opensource.org/licenses/MIT)