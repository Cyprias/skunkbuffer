(function() {
	var core        = SkunkBuffer10;

	var states = core.states;


	core.ignorePlayerForFellowship = {};
	states["Start"] = function onExecute(params, callback) {
		var session = params.session;
		var executeTime = params.executeTime;
		if (executeTime && executeTime != core.lastExecutionTime) return; // pop(new core.Error("STATE_CHANGE"));
		//if (core.getTopSession() != session) return;
		//core.debug("<onExecuteStart>");
		
		
		if (!skapi.szFellowship) {
			core.info("We should create a fellowship.");
			return core.createFellowship({name: core.config.profile.fellowshipName}, callback);
		} else {
			if (core.fellowshipInfo && core.fellowshipInfo.openFellow != 1) {
				var leader = core.fellowshipInfo.leader;
				core.info("leader: " + leader);
				if (leader == skapi.acoChar.oid) {
					core.info("Opening fellowship...");
					return core.openFellowship(undefined, callback);
				}
			}
		}

		var fellowship = core.getFellowshipSkapi();
		//core.console("fellowship: " + JSON.stringify(fellowship));
		
		var candidates = core.getPlayers().filter(function(aco) {
			return core.config.profile.fellowship.some(function(info) {
				return info.name == aco.szName;
			});
		}).filter(function(aco) {
			return !fellowship.some(function(aco2) {
				return aco.szName == aco2.szName;
			});
		}).filter(function(aco) {
			if (core.ignorePlayerForFellowship[aco.szName] && new Date().getTime() < core.ignorePlayerForFellowship[aco.szName]) {
				return false;
			}
			return true;
		});
		//core.info("candidates: " + candidates.length, candidates.join());

		if (candidates.length > 0) {
			var candidate = candidates[0];
			return core.requestCommandResult({
				name: candidate.szName,
				command: "sb fellowship"
			}, onFellowshipInfo);
			function onFellowshipInfo(err, json) {
				if (err) {
					if (err.message == "TIMED_OUT") {
						core.info(candidate.szName + " ignored our fellowship info request, ignoring them for a minute.");
						core.ignorePlayerForFellowship[candidate.szName] = new Date().getTime() + (60*1000);
						return callback();
					}
					
					core.warn("error", err);
					return callback(err);
				}

				var names;
				try {
					names = JSON.parse(json);
				} catch (err) {
					return callback(err);
				}
				
				var theirSize = names.length;
				var mySize = fellowship.length;
				var combinedSize = mySize + theirSize;
				
				if (combinedSize <= 9 && theirSize >= mySize) {
					// Join theirs, it's larger.
					if (!skapi.szFellowship) {
						return onQuit();
					}
					return core.quitFellowship(undefined, onQuit);
				}

				var mineWithoutMe = fellowship.length - 1;
				var theirsWithMe = theirSize + 1;
				
				core.debug("mineWithoutMe: " + mineWithoutMe, "theirsWithMe: " + theirsWithMe);

				if (theirsWithMe <= mineWithoutMe) {
					// Theirs will be bigger than mine without me. Join them.
					if (!skapi.szFellowship) {
						return onQuit();
					}
					return core.quitFellowship(undefined, onQuit);
				}
				
				core.info("Our fellowships are too big, ignoring.");
				core.ignorePlayerForFellowship[candidate.szName] = new Date().getTime() + (5*60*1000);
				callback();
			}
			function onQuit(err) {
				if (err) return callback(err);
				core.info("asking " + candidate.szName + " to recruit us...");
				core.tell({
					szRecipient: candidate.szName, 
					szMsg      : "sb recruit"
				});
				core.ignorePlayerForFellowship[candidate.szName] = new Date().getTime() + 1000 * 30;
				core.waitForFellowshipRecruit(undefined, callback);
			}
		}


		var staminaPercentage = core.getStaminaPercentage();
		var manaPercentage = core.getManaPercentage()
		if (staminaPercentage < .8) {
			var spell = core.getHighestKnownCommonSpell({commonName: "Revitalize Self"});
			if (spell) {
				return core.doCastSpell({
					spellid: spell.spellid
				}, callback);
			}
		}

		if (manaPercentage < .2) {
			var spell = core.getHighestKnownCommonSpell({commonName: "Stamina to Mana Self"});
			if (spell) {
				return core.doCastSpell({
					spellid: spell.spellid
				}, callback);
			}
		}


		var buffs = core.getMyBuffs();
		var wantedBuffs = Object.keys(core.config.profile.selfBuffs).filter(function(szName) {
			return core.config.profile.selfBuffs[szName] == true;
		});
		
		var neededBuff = wantedBuffs.find(function(szName) {
			var spells = core.getSpellsFromCommonName({commonName: szName, logger:core.console}).filter(function(spell) {
				return skapi.FSpellidInSpellbook(spell.spellid)
			});
			if (spells.length == 0) return false;
			var highest = spells[spells.length-1];
			var found = buffs.find(function(spell, i) {
				return spell.szName == highest.szName;
			});
			if (!found) return true;
			
			//core.info("found.csecRemain: " + found.csecRemain);
			
			return found.csecRemain < core.config.profile.rebuffRemainingDuration;
		});

		if (neededBuff) {
			var spell = core.getHighestKnownCommonSpell({commonName: neededBuff});
			if (spell) {
				
				//core.info("Pushing to cast " + spell.spellid);
				return core.pushState(core.createSession({state: "Cast", spellid: spell.spellid}));
				
				/*
				return core.castSpell({spellid  : spell.spellid}, function onCastBuff(err, results) {
					if (err) return callback(err);
					callback();
				});
				*/
			}
		}
		
		if (core.summonQueue.length > 0) {
			var request = core.summonQueue[0];
			var spellid;
			var name;
			var heading;
			if (request.isPrimary == true) {
				spellid = skapi.SpellidFromSz("Summon Primary Portal I");
				name = core.config.character.primaryTie;
				heading = core.config.character.primaryTieHeading || 0;
			} else if (request.isSecondary == true) {
				spellid = skapi.SpellidFromSz("Summon Secondary Portal I");
				name = core.config.character.secondaryTie;
				heading = core.config.character.secondaryTieHeading || 0;
			}
			
			return core.face({heading: heading}, onFace);
			function onFace(err, results) {
				if (err) {
					core.tell({
						szRecipient: request.requester, 
						szMsg      : "// Unhandled error occured: " + err.message
					});
					core.summonQueue.splice(0, 1);
					return callback();
				}
				
				return core.doCastSpell({
					spellid: spellid
				}, onSummon);
			}
			function onSummon(err, results) {
				if (err) {
					if (err.message == "SET_COMBAT_TIMED_OUT") return callback();
					core.tell({
						szRecipient: request.requester, 
						szMsg      : "// Unhandled error occured: " + err.message
					});
					core.summonQueue.splice(0, 1);
					return callback();
				}
				core.summonQueue.splice(0, 1);
				core.tell({szRecipient: request.requester, szMsg: "// Portal to " + name + " opened."})
				return callback();
			}
		}
		
		if (core.buffQueue.length > 0) {
			var request = core.buffQueue[0];
			
			var acoRequester = skapi.AcoFromSz(request.requester);
			//core.info("acoRequester: " + acoRequester);
			if (!acoRequester || !acoRequester.fExists) {
				if (request.primaryBot == skapi.acoChar.szName) {
					core.tell({szRecipient: request.requester, szMsg: "// You've moved too far away, removing you from queue."});
				}
				core.buffQueue.splice(0, 1);
				return callback();
			}
			
			if (skapi.maplocCur.Dist3DToMaploc(acoRequester.maploc) > 25/240) {
				if (request.primaryBot == skapi.acoChar.szName) {
					core.tell({szRecipient: request.requester, szMsg: "// You're too far away, removing you from queue."});
				}
				core.buffQueue.splice(0, 1);
				return callback();
			}
			
			//core.console("request: " + JSON.stringify(request, undefined, "\t"));
			if (request.spells.length == 0) {
				core.info("We're done " + request.requester + "'s buffs.");
				
				if (request.primaryBot == skapi.acoChar.szName) {
					var elapsed = new Date().getTime() - request.requestTime;
					core.tell({szRecipient: request.requester, szMsg: "// Request completed after " + Math.floor(elapsed / 1000) + " seconds."});
				}
				
				core.buffQueue.splice(0, 1);
				return callback();;
			}
			
			var randomSpell = request.spells[Math.floor(Math.random()*request.spells.length)];
			//core.info("randomSpell: " + randomSpell);
			
			var spell = core.getHighestKnownCommonSpell({commonName: randomSpell, logger: core.console});
			if (!spell) return callback();
			//core.info("randomSpell: " + randomSpell, "spell: " + spell.szName);
			
			//var session = core.createSession({state: "Cast", spellid: spell.spellid});
			
			if (request.primaryBot == skapi.acoChar.szName) {
				if (request.startTime == 0) {
					request.startTime = new Date().getTime();
					core.tell({szRecipient: request.requester, szMsg: "// Starting your buffs..."});
				}
			}

			core.debug("Casting " + randomSpell + " (" + spell.szName + ") on " + request.requester + "...");
			return core.doCastSpell({
				spellid: spell.spellid,
				aco: acoRequester
			}, function(err, results) {
				if (err) {
					if (err.message == "SPELL_OUT_OF_RANGE") {
						//var index = request.spells.indexOf(randomSpell);
						//if (index >= 0) request.spells.splice(index, 1);
						//return callback();
						core.info("Target is too far", "3d: " + (skapi.maplocCur.Dist3DToMaploc(acoRequester.maploc) * 240));
						
					} else if (err.message == "CAST_SPELL_TIMED_OUT") {
						if (spell.skid == skidItemEnchantment) {
							core.info("timed out on item spell, success?");
							return onSuccess();
						}
					}

					return callback(err);
				}
				onSuccess();
			});
			
			function onSuccess() {
				var index = request.spells.indexOf(randomSpell);
				if (index >= 0) {
					request.spells.splice(index, 1);
				}
				core.status.buffsCast += 1;

				if (core.config.profile.broadcastRequestsToFellowship == true && skapi.cofellow.Count > 1) {
					core.chat({
						chrm : chrmFellowship,
						szMsg: "sb cb " + JSON.stringify([request.requester, spell.spellid]) // 
					});
				}

				callback();
			}
		}
		
		//core.info("core.fCombatMode: " + core.fCombatMode);
		if (core.fCombatMode == true) {
			return core.setCombatMode({mode: false}, callback);
		}
		
		if (core.shouldStackInventory()) {
			return core.stackInventory(undefined, callback);
		}

		core.setTimeout(callback, 1000);
	};
	
})();	