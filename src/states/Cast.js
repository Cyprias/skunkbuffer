 (function() {
	var core        = SkunkBuffer10;

	var states = core.states;



	states["Cast"] = function onExecute(params, callback) {
		var session = params.session;
		var executeTime = params.executeTime;
		if (executeTime && executeTime != core.lastExecutionTime) return; // pop(new core.Error("STATE_CHANGE"));
		//if (core.getTopSession() != session) return;
		//core.info("<onExecuteCast>");
		var spellid = session.spellid;
		var aco = session.aco;
		
		/*
		if (core.fCombatMode != true) {
			return core.setCombatMode({mode: true}, function onCombat(err, results) {
				if (err) return callback(err);
				callback();
			});
		}

		return core.castSpell({spellid: spellid, acoTarget: acoTarget}, function onCast(err, results) {
			if (err) {
				core.warn("Error casting spell", err);
				return callback(err);
			}
			core.popState(session, undefined, true);
		});
		*/
		return core.doCastSpell({
			spellid: spellid,
			aco: aco
		}, function(err, results) {
			if (err) return callback(err);
			core.popState(session, undefined, true);
		});
	};
	
})();	