/********************************************************************************************\
	Script:         SkunkBuffer-1.0
	Purpose:        Skunkworks buff bot script for Asheron's Call emulator server.
	Author:         Cyprias
	Date:           2021/07/17
	Licence:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "SkunkBuffer-1.0";
var MINOR = 211018; // Year Month Day

(function (factory) {
	var params = {};
	params.EventEmitter   = (typeof EventEmitter !== "undefined" && EventEmitter) || require("modules\\SkunkSuite\\EventEmitter"); 
	
	// eslint-disable-next-line no-undef
	params.SkunkSchema11 = (typeof SkunkSchema11 !== "undefined" && SkunkSchema11);
	
	// eslint-disable-next-line no-undef
	params.SkunkLayout10 = (typeof SkunkLayout10 !== "undefined" && SkunkLayout10);
	
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(params);
	} else {
		// eslint-disable-next-line no-undef
		SkunkBuffer10 = factory(params); // Global object.
	}
}(function (params) {
	var EventEmitter = params.EventEmitter;
	var SkunkSchema11 = params.SkunkSchema11;
	var SkunkLayout10 = params.SkunkLayout10;
	
	var core = LibStub("SkunkScript-1.0").newScript(new EventEmitter(), 
		MAJOR, 
		"SkunkTimer-1.0", 
		"SkunkLogger-1.0", 
		"SkunkSchema-1.1", 
		"SkunkActions-1.0",
		"SkunkConfig-1.0",
		"SkunkEvent-1.0",
		"SkunkFS-1.0",
		"SkunkComm-1.0");

	core.setShortName("SB");
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	//core.Error = Error;
	
	core.version         = MAJOR + "." + MINOR; 
	core.title           = core.version;

	core.states = {};

	var ONE_SECOND = 1000;
	var ICON_START = 0x6000000;
	
	var defaultConfig = {
		profile: {
			selfBuffs                    : {},
			rebuffRemainingDuration      : 60,
			broadcastRequestsToFellowship: false,
			fellowship                   : [],
			fellowshipName               : "Hello",
			advertisements               : []
		},
		character: {
			active             : false,
			primaryTie         : "",
			secondaryTie       : "",
			primaryTieHeading  : 0,
			secondaryTieHeading: 0
		}
	};

	core.buffQueue = [];
	core.summonQueue = [];
	
	core.status = {};
	core.status.startTime = new Date().getTime();
	core.status.buffRequests = 0;
	core.status.summonRequests = 0;
	core.status.buffsCast = 0;
	

	core.main = function main() {
		if (skapi.plig == pligNotRunning) {
			core.console("****************************************************");
			core.console("Asheron's call isn't running. Cannot launch script.");
			core.console("****************************************************");
			return console.StopScript();
		} else if (skapi.plig == pligAtLogin) {
			core.console("****************************************************");
			core.console("A character hasn't logged in yet. Cannot launch script.");
			core.console("****************************************************");
			return console.StopScript();
		}

		core.initialize();

		// SkunkWorks can launch scripts at the character script, so wait until you're in world before proceeding)
		while (skapi.plig != pligInWorld) {
			core.console("Waiting to finish entering world... (plig: " + skapi.plig + ")");
			skapi.WaitEvent(ONE_SECOND, wemFullTimeout);
			
		}

		core.enable();

		while (core._script.enabledState == true) {
			skapi.WaitEvent(100, wemFullTimeout);
			
			if (skapi.plig == pligNotRunning) {
				core.warn("AC has closed, shutting down... (plig: " + skapi.plig + ")");
				core.disable();
				break;
			}
		}

		// Tell SkunkWorks to remove all panels except its own.
		skapi.RemoveControls("!SkunkWorks");
		
		skapi.OutputLine("console.StopScript()", opmConsole);
		// eslint-disable-next-line no-console
		console.StopScript();
	};
	
	var SkunkComm;
	var AnimationFilter;
	core.on("onInitialize", function onInitialize() {
		core.debug("<onInitialize>");
		
		core.config = new core.Config("./configs", defaultConfig).load()
			.save();

		AnimationFilter = LibStub("AnimationFilter-1.0");
		SkunkComm = LibStub("SkunkComm-1.0");
		
		
		core.commonSpellNames = core.getJsonFile({path: "./src/data/commonSpellNames.json"});

		core.loadUILayout();

		skapi.SetIdleTime(60 * 60 * 24 * 7);

		//generateCommonSpells();
	});

	core.getSpellsFromCommonName = function getSpellsFromCommonName(params) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var commonName = params.commonName;
		
		var spells = core.commonSpellNames[commonName];
		if (!spells) {
			logger("No spells for " + commonName);
			throw new core.Error("UNKNOWN_COMMON_NAME").setCode(0x7b0b1827);
		}
		
		spells = spells.map(function(szName) {
			var spellid = skapi.SpellidFromSz(szName);

			//logger("szName: " + szName, "spellid: " + spellid);
			return spellid && skapi.SpellInfoFromSpellid(spellid);
		});
		
		spells.forEach(function(spell) {
			//logger("spell: " + spell.szName);
		});
		
		spells.sort(function(a, b) {
			return a.diff - b.diff;
		});

		//logger("spell: " + spells.map(function(spell) {
		//	return spell.szName;
		//}).join());
		
		return spells;
	};
	
	core.filterKnownSpells = function filterKnownSpells(spells) {
		return spells.filter(function(spell) {
			skapi.FSpellidInSpellbook(spell.spellid);
		});
	};
	
	core.getHighestKnownCommonSpell = function getHighestKnownCommonSpell(params) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var commonName = params.commonName;
		
		var spells = core.getSpellsFromCommonName({logger: logger, commonName: commonName}).filter(function(spell) {
			return skapi.FSpellidInSpellbook(spell.spellid);
		});
		logger(commonName + " has " + spells.length + " spells.");
		if (spells.length == 0) return;
		return spells[spells.length - 1];
	};

	core.getJsonFile = function getJsonFile(params) {
		if (core.existsSync(params.path)) {
			var json = core.readFileSync(params.path);
			var items;
			try {
				items = JSON.parse(json);
			} catch (e) {
				throw e;
			}
			return items;
		}
	};

	core.isFamilyKnown = function isFamilyKnown(spell) {
		var cospell = skapi.CospellFromFamily(spell.family);
		var ispell;
		for (var i = 0; i < cospell.Count; i++) {
			ispell = cospell.Item(i);
			if (skapi.FSpellidInSpellbook(ispell.spellid) == true) {
				return true;
			}
		}
		return false;
	};

	function generateCommonSpells() {
		var commonName;
		var start = new Date().getTime();
		
		var commonNames = {};
		
		var spell;
		for (var i = 0; i < skapi.spellidMax; i++) {
			spell = skapi.SpellInfoFromSpellid(i);
			if (!spell) continue;
			
			if (!core.isFamilyKnown(spell)) continue;
			
			if (spell.szName.match(/(.*) (I|II|III|IV|V|VI|VII)$/)) {
				commonName = RegExp.$1;
				
				commonNames[commonName] = commonNames[commonName] || [];

				//commonNames[commonName].push(spell.szName);
				commonNames[commonName].push(spell.szName); //  + ": " + spell.szDesc
				continue;
			}

			commonName = core.getCommonName(spell);
			if (!commonName) {
				//core.info("couldn't get common name for " + spell.szName);
				continue;
			}
			commonNames[commonName] = commonNames[commonName] || [];
			commonNames[commonName].push(spell.szName); //  + ": " + spell.szDesc
		}
		
		var elapsed = new Date().getTime() - start;
		core.console("elapsed: " + elapsed);

		core.writeFileSync("./commonSpellNames.json", JSON.stringify(commonNames, undefined, "\t"));
	}
	

	core.getCommonName = function getCommonName(spell) {
		var cospell = skapi.CospellFromFamily(spell.family);
		if (cospell.Count == 0) return;
		
		var descriptions = {};

		var ispell;
		for (var i = 0; i < cospell.Count; i++) {
			ispell = cospell.Item(i);
			if (ispell.spellid == spell.spellid) continue;
			if (ispell.szName.match(/(.*) (I|II|III|IV|V|VI|VII)$/)) {
				descriptions[ispell.szDesc] = RegExp.$1;
			}
		}
		if (Object.keys(descriptions).length == 0) return;
		
		var match = core.propose({word: spell.szDesc, dictionary: Object.keys(descriptions)});
		if (!match) return;
		
		return descriptions[match];
	};

	core.propose = function propose(params) {
		var word = params.word;
		var dictionary = params.dictionary;
		var threshold = (!params || params.threshold === undefined ? 0 : params.threshold);
		var ignoreCase = (!params || params.ignoreCase === undefined ? false : params.ignoreCase);

		var ratio;
		var distance;
		var proposed;
		var max_ratio = 0;

		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < dictionary.length; ++i) {
			if (ignoreCase)
				distance = core.levenshtein(word, dictionary[i], true);
			else
				distance = core.levenshtein(word, dictionary[i]);

			if (distance > word.length)
				ratio = 1 - distance / dictionary[i].length;
			else
				ratio = 1 - distance / word.length;

			if (ratio > max_ratio) {
				max_ratio = ratio;
				proposed = dictionary[i];
			}
		}

		if (max_ratio >= threshold)
			return proposed;

		return null;
	};

	core.levenshtein = function factory() {
		// https://github.com/words/levenshtein-edit-distance/blob/master/license
		var cache = [];
		var codes = [];
		return function levenshtein(value, other, insensitive) {
			var length;
			var lengthOther;
			var code;
			var result;
			var distance;
			var distanceOther;
			var index;
			var indexOther;

			if (value === other) {
				return 0;
			}

			length = value.length;
			lengthOther = other.length;

			if (length === 0) {
				return lengthOther;
			}

			if (lengthOther === 0) {
				return length;
			}

			if (insensitive) {
				value = value.toLowerCase();
				other = other.toLowerCase();
			}

			index = 0;

			// eslint-disable-next-line no-restricted-syntax
			while (index < length) {
				codes[index] = value.charCodeAt(index);
				cache[index] = ++index;
			}

			indexOther = 0;

			// eslint-disable-next-line no-restricted-syntax
			while (indexOther < lengthOther) {
				code = other.charCodeAt(indexOther);
				result = distance = indexOther++;
				index = -1;

				// eslint-disable-next-line no-restricted-syntax
				while (++index < length) {
					distanceOther = code === codes[index] ? distance : distance + 1;
					distance = cache[index];
					cache[index] = result =
						distance > result
							? distanceOther > result
								? result + 1
								: distanceOther
							: distanceOther > distance
								? distance + 1
								: distanceOther;
				}
			}

			return result;
		};
	}();

	core.getSpellsByCommonName = function getSpellsByCommonName(params) {
		var spells = [];

		return spells;
	};

	
	core.on("onEnable", function onEnable() {
		core.addHandler(evidOnCommand);
		core.addHandler(evidOnEndPortalSelf);
		core.addHandler(evidOnEnd3D);
		core.addHandler(evidOnChatBoxMessage);

		//core.addHandler(evidOnAddToInventory);
		
		SkunkComm.emitter.on("onMessage", core.onMessage);
		
		
		AnimationFilter.addCallback(core.OnAnimation);
		FellowshipFilter10.addCallback(core.onFellowship);
		
		if (skapi.plig == pligInWorld) {
			core.showGUI();
		}
		
		if (core.config.character.active == true) {
			core.emit("onActivate");
		}
		
		function adTimer() {
			var ads = core.config.profile.advertisements.filter(function(info) {
				return info.enabled;
			});
			if (ads.length == 0) return;
			var randomAd = ads[Math.floor(Math.random()*ads.length)];
			core.announce(randomAd.text);
		}
		core.setInterval(adTimer, 10*60*1000);
		//adTimer();
	});

	core.OnEnd3D = function OnEnd3D() {
		core.trace("<OnEnd3D>");
		core.disable();
	};

	core.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
		if (cmc == cmcGreen) { // 0
			if (szMsg.match(/Logging off.../)) {
				core.warn("Client is logging off!");
				core.disable();
				return;
			}
		}
	};

	core.announce = function announce(szText) {
		var szText = core.trimStr(szText);

		// Check if the text starts with a @, use skapi.Keys() to input the message. Decal plugins don't seem to catch commands via skapi.InvokeChatParser().
		if (szText.match(/^@/)) {
			// Check if the user is typing something, forgo pasting command into chat.
			if (skapi.fInChatBuffer) {
				return;
			}
			skapi.Keys("{return}");
			skapi.PasteSz(szText);
			skapi.Keys("{return}");
			return;
		}

		// Check if ad is a emote, don't add suffix.
		if (!szText.match(/^\*(.*)\*$/)) {
			szText += ' -b-';
		}
		
		return skapi.InvokeChatParser(szText);
	};

	core.fellowshipInfo = null;
	core.onFellowship = function onFellowship(payload) {
		core.console("<onFellowship>", JSON.stringify(payload, undefined, "\t"));
		core.fellowshipInfo = payload;
	};

	core.on("onDisable", function onDisable() {
		AnimationFilter.removeCallback(core.OnAnimation);
	});
	
	core.arrayTemplate = function arrayTemplate(arr, data) {
		if (typeof data === "undefined") return arr;
		return arr.map(function(value) {
			return core.stringTemplate(value, data);
		});
	};
	
	core.getItemCount = function getItemCount(params) {
		var name = params.name;
		var acf = skapi.AcfNew();
		acf.szName = name;
		acf.olc = olcInventory;
		var items = core.coacoToArray(acf.CoacoGet());
		return items.reduce(function(tally, aco) {
			return tally += aco.citemStack;
		}, 0);
	};
	
	core.arrayUnique = function arrayUnique(array) {
		var a = array.concat();
		for(var i = 0; i < a.length; ++i) {
			for(var j = i + 1; j < a.length; ++j) {
				if(a[i] === a[j])
					a.splice(j--, 1);
			}
		}

		return a;
	};
	
	core.onMessage = function onMessage(params) {
		var evid        = params.evid;
		var szSender    = params.szSender;
		var szMsg       = params.szMsg;
		var szChannel   = params.szChannel;
		
		if (szMsg.match(/^\/\//i)) return;
		
		//if (szSender == skapi.acoChar.szName) return;
		if (core.config.character.active != true) return;
		
		if (evid == evidOnTell) {
			
			//core.info("buffProfile: " + buffProfile);
			
			if (szMsg.match(/^help$/i)) {
				core.tell({szRecipient: szSender, szMsg: "// Available commands: help comps whereto primary secondary about"});
				core.tell({szRecipient: szSender, szMsg: "// Available buff profiles: mage finesse heavy light missile twohand weapon wand shield banes craft lockpick mule protects regens tink xpchain shield"});
				return;
			} else if (szMsg.match(/^comps$/i)) {
				var comps = {};
				comps["Prismatic Taper"] = core.getItemCount({name: "Prismatic Taper"});
				comps["Platinum Scarab"] = core.getItemCount({name: "Platinum Scarab"});
				comps["Pyreal Scarab"] = core.getItemCount({name: "Pyreal Scarab"});

				//comps["Gold Scarab"] = core.getItemCount({name:"Gold Scarab"});
				comps["Silver Scarab"] = core.getItemCount({name: "Silver Scarab"});
				comps["Mana Scarab"] = core.getItemCount({name: "Mana Scarab"});
				
				var sorted = Object.keys(comps).map(function(szName) {
					return {
						szName: szName,
						amount: comps[szName]
					};
				});
				sorted.sort(function(a, b) {
					if (a.szName < b.szName) return -1;
					if (a.szName > b.szName) return -1;
					return 0;
				});
				core.tell({szRecipient: szSender, szMsg      : "// " + szMsg + ": " + sorted.map(function(info) {
					return info.szName + ": " + info.amount;
				}).join(", ")});
				return;
			} else if (szMsg.match(/^sb comps$/i)) {
				var comps = {};
				comps["Prismatic Taper"] = core.getItemCount({name: "Prismatic Taper"});
				comps["Platinum Scarab"] = core.getItemCount({name: "Platinum Scarab"});
				comps["Pyreal Scarab"] = core.getItemCount({name: "Pyreal Scarab"});
				comps["Silver Scarab"] = core.getItemCount({name: "Silver Scarab"});
				comps["Mana Scarab"] = core.getItemCount({name: "Mana Scarab"});
				return core.tell({szRecipient: szSender, szMsg      : "// " + szMsg + ": " + JSON.stringify(comps)});
			} else if (szMsg.match(/^whereto$/i)) {
				var names = [];
				if (core.config.character.primaryTie && core.config.character.primaryTie != "") {
					names.push("primary: " + core.config.character.primaryTie);
				}
				if (core.config.character.secondaryTie && core.config.character.secondaryTie != "") {
					names.push("secondary: " + core.config.character.secondaryTie);
				}
				if (names.length == 0) {
					return core.tell({szRecipient: szSender, szMsg: "// I have no ties."});
				}
				return core.tell({szRecipient: szSender, szMsg: "// " + names.join(", ")});
			} else if (szMsg.match(/^primary$/i)) {
				if (core.config.character.primaryTie == "") {
					return core.tell({szRecipient: szSender, szMsg: "// I cannot summon primary."});
				}
				
				core.summonQueue.push({
					requester  : szSender,
					isPrimary  : true,
					isSecondary: false,
					requestTime: new Date().getTime(),
					startTime  : 0
				});
				core.status.summonRequests += 1;
				return core.tell({szRecipient: szSender, szMsg: "// Summon request queued."});
			} else if (szMsg.match(/^secondary$/i)) {
				if (core.config.character.secondaryTie == "") {
					return core.tell({szRecipient: szSender, szMsg: "// I cannot summon secondary."});
				}
				
				core.summonQueue.push({
					requester  : szSender,
					isPrimary  : false,
					isSecondary: true,
					requestTime: new Date().getTime(),
					startTime  : 0
				});
				core.status.summonRequests += 1;
				return core.tell({szRecipient: szSender, szMsg: "// Summon request queued."});
			} else if (szMsg.match(/^sb fellowship$/i)) {
				if (!core.isOnOurFellowList) {
					return core.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": NOT_ALLOWED"});
				}
				var names = core.getFellowshipSkapi().map(function(aco) {
					return aco.szName;
				});
				return core.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": " + JSON.stringify(names)});
			} else if (szMsg.match(/^sb recruit$/i)) {
				if (!core.isOnOurFellowList) {
					return core.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": NOT_ALLOWED"});
				}
				
				var acoPlayer = skapi.AcoFromSz(szSender);
				if (!acoPlayer || !acoPlayer.fExists) {
					return core.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": CANT_SEE_YOU"});
				}
				
				core.info("Attempting to recruit " + szSender + "...");
				return core.recruitPlayer({
					aco: acoPlayer
				}, function onRecruit(err, results) {
					core.debug("<onRecruit>", err, results);
					if (err) {
						return SkunkComm.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": " + err.message});
					}
					return SkunkComm.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": SUCCESS"});
				});
			} else if (szMsg.match(/^sb queue$/i)) {
				var totalSpells = core.buffQueue.reduce(function(tally, queue) {
					return tally += queue.spells.length;
				}, 0);
				return SkunkComm.tell({szRecipient: szSender, szMsg: "// " + szMsg + ": " + JSON.stringify([core.buffQueue.length, totalSpells])});
			} else if (szMsg.match(/^about$/i)) {
				return core.tell({szRecipient: szSender, szMsg: "// I'm running " + core.version + ", https://gitlab.com/Cyprias/SkunkBuffer"});
			} else if (szMsg.match(/^cancel$/i)) {
				
				if (core.config.profile.broadcastRequestsToFellowship == true && skapi.cofellow.Count > 1) {
					core.chat({
						chrm : chrmFellowship,
						szMsg: "sb cancel " + JSON.stringify([szSender])
					});
				}
				
				core.buffQueue = core.buffQueue.filter(function(info) {
					return info.requester != szSender;
				});
				
				return core.tell({szRecipient: szSender, szMsg: "// Canceled your buff requests."});
				return;
			}
			
			var lowerMsg = szMsg.toLowerCase();
			core.info("lowerMsg: " + lowerMsg);

			if (core.buffProfiles[lowerMsg]) {
				var spells = core.buffProfiles[lowerMsg].slice();

				var before = spells.length;
				var spells = core.arrayUnique(spells);
				if (before != spells.length) {
					core.info("spell diff", before, spells.length);
				}

				core.buffQueue.push({
					requester  : szSender,
					spells     : spells,
					primaryBot : skapi.acoChar.szName,
					requestTime: new Date().getTime(),
					startTime  : 0
				});
				core.status.buffRequests += 1;
				
				if (core.config.profile.broadcastRequestsToFellowship == true && skapi.cofellow.Count > 1 && spells.length >= skapi.cofellow.Count) {
					var compactIds = spells.map(function(commonName) {
						var spells = core.commonSpellNames[commonName];
						if (!spells) return;
						return skapi.SpellidFromSz(spells[0]);
					});
					core.chat({
						chrm : chrmFellowship,
						szMsg: "sb br " + JSON.stringify([szSender, compactIds]) // 
					});
				}
				
				core.tell({szRecipient: szSender, szMsg: "// Queued " + spells.length + " buffs for you."});
				return;
			}
			
			var spells = [];
			var words = lowerMsg.split(" ");
			var allProfiles = true;
			for (var i = 0; i < words.length; i++) {
				if (core.buffProfiles[words[i]]) {
					spells = spells.concat(core.buffProfiles[words[i]].slice());
					continue;
				}
				
				allProfiles = false;
				break;
			}
			
			if (allProfiles != true) {
				core.tell({szRecipient: szSender, szMsg: "// Unknown request: " + szMsg});
				return;
			}

			var before = spells.length;
			var spells = core.arrayUnique(spells);
			if (before != spells.length) {
				core.info("spell diff", before, spells.length);
			}

			// TODO: Remove spells already in queue.
			
			core.buffQueue.push({
				requester  : szSender,
				spells     : spells,
				primaryBot : skapi.acoChar.szName,
				requestTime: new Date().getTime(),
				startTime  : false
			});
			core.status.buffRequests += 1;
			
			if (core.config.profile.broadcastRequestsToFellowship == true && skapi.cofellow.Count > 1) {
				var compactIds = spells.map(function(commonName) {
					var spells = core.commonSpellNames[commonName];
					if (!spells) return;
					return skapi.SpellidFromSz(spells[0]);
				});
				compactIds.sort(function (a, b) {
					return a - b;
				});
				core.chat({
					chrm : chrmFellowship,
					szMsg: "sb br " + JSON.stringify([szSender, compactIds]) // 
				});
			}

			core.tell({szRecipient: szSender, szMsg: "// Queued " + spells.length + " buffs for you."});
			
		} else if (evid == evidOnTellFellowship) {
			if (szSender == skapi.acoChar.szName) return;
			
			if (szMsg.match(/sb br (.*)/)) { // buff request
				
			
				var json = RegExp.$1;
				var data;
				try {
					data = JSON.parse(json);
				} catch (err) {
					core.warn("Error parsing json", err);
					return;
				}
				
				var szRequester = data[0];
				var compactIds = data[1];
				
				core.info("szRequester: " + szRequester);
				core.info("compactIds: " + compactIds.length);
				
				var commonNames = compactIds.map(function(spellid) {
					var spell = skapi.SpellInfoFromSpellid(spellid);
					return core.getCommonName(spell);
				});
				
				//core.console("commonNames: " + commonNames.join());
				
				core.buffQueue.push({
					requester : szRequester,
					spells    : commonNames,
					primaryBot: szSender
				});
				
				return;
			} else if (szMsg.match(/sb cb (.*)/)) { // cast spell
				var json = RegExp.$1;
				var data;
				try {
					data = JSON.parse(json);
				} catch (err) {
					core.warn("Error parsing json", err);
					return;
				}
				var szTarget = data[0];
				var spellid = data[1];
				
				var spell = skapi.SpellInfoFromSpellid(spellid);
				
				
				var commonName = core.getCommonName(spell);
				core.debug(szSender + " cast " + spell.szName + " on " + szTarget, "commonName: " + commonName);
				
				
				core.buffQueue.forEach(function(queue) {
					if (queue.requester != szTarget) return;
					
					//core.info(szTarget + " has " + queue.spells.length + " spells remaining.");
					
					//var match = queue.find(function(commonName2) {
					//	return commonName == commonName2;
					//});
					
					var index = queue.spells.indexOf(commonName);
					if (index >= 0) {
						core.debug("B Removing " + commonName + " from " + queue.requester + "'s queue.", index);
						queue.spells.splice(index, 1);
					}
					
				});
				
				return;
				
			} else if (szMsg.match(/^comps$/i)) {
				var comps = {};
				comps["Prismatic Taper"] = core.getItemCount({name: "Prismatic Taper"});
				comps["Platinum Scarab"] = core.getItemCount({name: "Platinum Scarab"});
				comps["Pyreal Scarab"] = core.getItemCount({name: "Pyreal Scarab"});

				//comps["Gold Scarab"] = core.getItemCount({name:"Gold Scarab"});
				comps["Silver Scarab"] = core.getItemCount({name: "Silver Scarab"});
				comps["Mana Scarab"] = core.getItemCount({name: "Mana Scarab"});
				
				var sorted = Object.keys(comps).map(function(szName) {
					return {
						szName: szName,
						amount: comps[szName]
					};
				});
				sorted.sort(function(a, b) {
					if (a.szName < b.szName) return -1;
					if (a.szName > b.szName) return -1;
					return 0;
				});
				core.tell({szRecipient: szSender, szMsg      : "// " + szMsg + ": " + sorted.map(function(info) {
					return info.szName + ": " + info.amount;
				}).join(", ")});
				return;
			} else if (szMsg.match(/^sb comps$/i)) {
				var comps = {};
				comps["Prismatic Taper"] = core.getItemCount({name: "Prismatic Taper"});
				comps["Platinum Scarab"] = core.getItemCount({name: "Platinum Scarab"});
				comps["Pyreal Scarab"] = core.getItemCount({name: "Pyreal Scarab"});
				comps["Silver Scarab"] = core.getItemCount({name: "Silver Scarab"});
				comps["Mana Scarab"] = core.getItemCount({name: "Mana Scarab"});
				return core.tell({szRecipient: szSender, szMsg      : "// " + szMsg + ": " + JSON.stringify(comps)});
			} else if (szMsg.match(/^sb cancel (.*)$/i)) {
				var json = RegExp.$1;
				var data;
				try {
					data = JSON.parse(json);
				} catch (err) {
					core.warn("Error parsing json", err);
					return;
				}
				var who = data.length >= 1 && data[0];
				
				core.buffQueue = core.buffQueue.filter(function(info) {
					return info.requester != who;
				});
				
				core.info("Removed " + who + " from queue.");
			}
		}
		
		core.debug("evid: " + evid, "szSender: " + szSender, "szMsg: " + szMsg, "szChannel: " + szChannel);
	};
	
	core.fCombatMode = false; // skapi.fCombatMode
	core.OnAnimation = function OnAnimation(payload) {
		if (payload.object == skapi.acoChar.oid) {
			if (payload.animation_type == AnimationFilter.animationTypes.GENERIC) {
				if (payload.stance == 73) {
					//core.info("We entered caster stance");
					core.fCombatMode = true;
					return;
				} else if (payload.stance == 61) {
					//core.info("We entered standing stance");
					core.fCombatMode = false;
					return;
				} else if (payload.stance == 60) {
					//core.info("We entered UA stance");
					core.fCombatMode = true;
					return;
				}
			}
		}
		
		//core.console("<OnAnimation>", JSON.stringify(payload, undefined, "\t"));
	};

	core.OnEndPortalSelf = function OnEndPortalSelf() {
		core.fCombatMode = false;
	};
	
	core.on("onActivate", function onActivate() {
		core.debug("<onActivate>");
		core.startTicking();
		
	});

	core.OnCommand = function OnCommand(szCmd) {
		core.debug("<OnCommand>", szCmd);
		szCmd = core.trimStr(szCmd);
		
		if (szCmd && szCmd.match(/^sb/i)) {
			if (szCmd.match(/^sb active (.*)/i)) {
				var rest = RegExp.$1;
				var value = (rest == "true" && true || false);
				core.config.character.active = value;
				if (value) {
					core.emit("onActivate", value);
				} else {
					core.emit("onDeactivate", value);
				}
				return;
			}
		}
	};

	var selfBuffs = [
		"Creature Enchantment Mastery Self", 
		"Strength Self",
		"Focus Self",
		"Willpower Self",
		"Life Magic Mastery Self",
		"Mana Conversion Mastery Self",
		"Item Enchantment Mastery Self",
		"Endurance Self",
		"Fealty Self",
		"Leadership Mastery Self",
		"Mana Renewal Self",
		"Regeneration Self",
		"Rejuvenation Self"
	];
	selfBuffs.sort();

	core.showGUI = function showGUI() {
		var viewTitle = core.title;
		var Element = SkunkSchema11.Element;
		
		var viewWidth = 250;
		var viewHeight = 250;

		var schema = Element("view", {title: viewTitle, icon: 1234, width: viewWidth, height: viewHeight + 32}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.Notebook', name: "nbMain", width: viewWidth, height: viewHeight - 20}, [
					Element("page", {label: "Status"}, [
						//Element("control", {progid:'DecalControls.FixedLayout'}, [
						//Element("control", {progid: 'DecalControls.PushButton', name: "xxx",   text: "xxx", width: 50, height: 50}) // width: 40, height: 20, top: 0, left: 0,  
						//])
						Element("control", {progid: 'DecalControls.list', name: "lstStatus"}, [
							Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth})
						])
					]),
					Element("page", {label: "Config"}, [
						Element("control", {progid: 'DecalControls.FixedLayout'}, [
							Element("control", {progid: 'DecalControls.Notebook', name: "nbConfig", width: viewWidth, height: viewHeight - 36}, [ 
								Element("page", {label: "Profile"}, [
									Element("control", {progid: 'DecalControls.FixedLayout'}, [
										Element("control", {progid: 'DecalControls.StaticText', text: "Profile: ", width: 100, height: 20}),
										Element("control", {progid: 'DecalControls.Choice', name: "chProfile", width: 140, height: 20, left: 50}),
										Element("control", {progid: 'DecalControls.PushButton', name: "btnNewProfile", width: 30, height: 20, left: 190, text: "New"}),
										Element("control", {progid: 'DecalControls.PushButton', name: "btnRemoveProfile", width: 30, height: 20, left: 220, text: "Del"}),
										Element("control", {progid: 'DecalControls.Notebook', name: "nbProfile", top: 20, width: viewWidth, height: viewHeight - 72}, [ 
											Element("page", {label: "General"}, [
												Element("control", {progid: 'DecalControls.list', name: "lstConfigProfileGeneral"}, [
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth - 60}),
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 30})
												])
											]),
											Element("page", {label: "Buff"}, [
												Element("control", {progid: 'DecalControls.list', name: "lstSelfBuffs"}, [
													Element("column", {progid: "DecalControls.CheckColumn"}),
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth - 60})
												])
											]),
											Element("page", {label: "Fellow"}, [
												Element("control", {progid: 'DecalControls.FixedLayout'}, [
													Element("control", {progid: 'DecalControls.list', name: "lstFellowship", width: viewWidth, height: viewHeight - 108}, [
														Element("column", {progid: "DecalControls.CheckColumn"}),
														Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth - 60}),
														Element("column", {progid: "DecalControls.IconColumn"})
													]),
													Element("control", {progid: 'DecalControls.PushButton', name: "btnAddFellow", width: 50, height: 20, left: 0, top: viewHeight - 108, text: "Add"})
												])
											]),
											Element("page", {label: "Ads"}, [
												Element("control", {progid: 'DecalControls.FixedLayout'}, [
													Element("control", {progid: 'DecalControls.list', name: "lstAdvertisements", width: viewWidth, height: viewHeight - 108}, [
														Element("column", {progid: "DecalControls.CheckColumn"}),
														Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth - 60}),
														Element("column", {progid: "DecalControls.IconColumn"})
													]),
													Element("control", {progid: 'DecalControls.PushButton', name: "btnAddAdvertisement", width: 50, height: 20, left: 0, top: viewHeight - 108, text: "Add"})
												])
											])
										])
									])
								]),
								Element("page", {label: "Character"}, [
									Element("control", {progid: 'DecalControls.FixedLayout'}, [
										Element("control", {progid: 'DecalControls.Notebook', name: "nbConfigCharacter", top: 0, width: viewWidth, height: viewHeight - 52}, [ 
											Element("page", {label: "General"}, [
												Element("control", {progid: 'DecalControls.list', name: "lstConfigCharactgerGeneral"}, [
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth - 60}),
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 30})
												])
											])
										])
									])
								])
							])
						])
					]),
					Element("page", {label: "Extras"}, [
						Element("control", {progid: 'DecalControls.FixedLayout'}, [
							Element("control", {progid: 'DecalControls.PushButton', name: "btnGiveComps",     width: 100, height: 20, top: 0, left: 0, text: "Give Comps"})
						])
					]),
					Element("page", {label: "Debug"}, [
						Element("control", {progid: 'DecalControls.list', name: "lstDebug"}, [
							Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth})
						])
					])
				]),

				Element("control", {progid: 'DecalControls.Checkbox', name: "chkActive", width: 100, height: 20, top: viewHeight - 20, left: 0,   text: "Active", checked: core.config.character.active && "True" || "False"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnQuit",     width: 40, height: 20, top: viewHeight - 20, left: 210, text: "Quit"})
			])
		]);
		
		var debugTests = [];
		debugTests.push({
			title: "Test",
			click: function() {
				core.info("Test!");
			}
		});

		debugTests.sort(function(a,b) {
			if (a.title < b.title) return -1;
			if (a.title > b.title) return 1;
			return 0;
		});
		
		function refreshDebugList() {
			skapi.GetControlProperty(viewTitle, "lstDebug", "Clear()");
			debugTests.forEach(function(test, i) {
				skapi.GetControlProperty(viewTitle, "lstDebug", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstDebug", "Data(" + 0 + ", " + i + ")", test.title);
			});
		};
		
		
		var shownSpellNames;
		function refreshBuffsList() {
			shownSpellNames = [];

			// lstSelfBuffs
			skapi.GetControlProperty(viewTitle, "lstSelfBuffs", "Clear()");

			//skapi.GetControlProperty(viewTitle, "lstSelfBuffs", "AddRow()");
			//skapi.SetControlProperty(viewTitle, "lstConfigProfileGeneral", "Data(" + 0 + ", " + col + ")", false);
			//skapi.SetControlProperty(viewTitle, "lstConfigProfileGeneral", "Data(" + 1 + ", " + col + ", 1)", 0);
			//skapi.SetControlProperty(viewTitle, "lstSelfBuffs", "Data(" + 0 + ", " + 0 + ")", "Setting");
			//skapi.SetControlProperty(viewTitle, "lstSelfBuffs", "Data(" + 1 + ", " + 0 + ")", "Spell Name");
			
			selfBuffs.forEach(function(szName, i) {
				skapi.GetControlProperty(viewTitle, "lstSelfBuffs", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstSelfBuffs", "Data(" + 0 + ", " + i + ")", core.config.profile.selfBuffs[szName] || false);
				skapi.SetControlProperty(viewTitle, "lstSelfBuffs", "Data(" + 1 + ", " + i + ")", szName);
				shownSpellNames.push(szName);
			});
			
		}
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != viewTitle) return;
			if (controlEvents[szControl]) return controlEvents[szControl](szPanel, szControl, dictSzValue);
			if (szControl == "nbMain") return;
			if (szControl == "nbProfile") return;
			core.info("<OnControlEvent>", szPanel, szControl, dictSzValue(szControl));
		};
		skapi.AddHandler(evidOnControlEvent, handler);

		var controlEvents = {};
		controlEvents["btnQuit"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			return core.disable();
		};
		controlEvents["chkActive"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl) == "True";
			core.config.character.active = value;
			core.config.saveCharacter();
			
			if (value == true) {
				core.info("Active");
				core.emit("onActivate");
			} else {
				core.info("Deactive");
				core.emit("onDeactivate");
			}
		};
		controlEvents["lstSelfBuffs"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl);
			var intCol = parseInt(value.split(",")[0], 10);
			var intRow = parseInt(value.split(",")[1], 10);
			var spellName = shownSpellNames[intRow];
			
			core.info("intCol: " + intCol, "intRow: " + intRow, "spellName: " + spellName);
			
			if (intCol == 0) {
				return SkunkSchema11.getControlPropertyAsync({
					panelName  : viewTitle,
					controlName: szControl,
					property   : "data(" + intCol + "," + intRow + ")"
				}, onSelfBuffsValue);
			}
			function onSelfBuffsValue(err, results) {
				if (err) core.warn("Error getting list value", err);
				core.config.profile.selfBuffs[spellName] = results;
				core.config.saveProfile();
			}
		};
		controlEvents["lstConfigProfileGeneral"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl);
			var intCol = parseInt(value.split(",")[0], 10);
			var intRow = parseInt(value.split(",")[1], 10);
			var info = configProfileList[intRow - 1];
			info.click();
		};
		controlEvents["lstConfigCharactgerGeneral"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl);
			var intCol = parseInt(value.split(",")[0], 10);
			var intRow = parseInt(value.split(",")[1], 10);
			var info = configCharacterGeneralList[intRow - 1];
			info.click();
		};
		
		controlEvents["btnNewProfile"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			return SkunkSchema11.showEditPrompt({
				title: "Profile name",
				value: ""
			}, function onValue(err, results) {
				if (err) return core.warn("Error asking for point value", err);
				if (results == "") return core.warn("Name cannnot be blank.");
				core.config.createProfile(results, defaultConfig);

				//refreshProfileMenu();
				populateProfileMenu();

				//refreshProfileGeneralList();
				refreshProfileLists();
				
			});
		};
		controlEvents["chProfile"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var name = dictSzValue(szControl);
			if (name == "" || name == core.config._keyProfile) return;

			//core.info("name: " + name);
			core.config.setProfileKey(name);
			core.config.loadProfile();

			//populateProfileMenu();
			//refreshProfileGeneralList();
			refreshProfileLists();
		};
		controlEvents["btnRemoveProfile"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var profileName;
			return SkunkSchema11.getControlPropertyAsync({
				panelName  : viewTitle,
				controlName: "chProfile",
				property   : "selected"
			}, onProfileName);
			function onProfileName(err, results) {
				if (err) core.warn("Error getting selected profile", err);
				profileName = listedProfileNames[results];
				core.info("profileName: " + profileName);
				if (profileName == "Default") {
					core.warn("Can't delete the Default profile.");
					return;
				}
				return SkunkSchema11.showConfirmationPrompt({
					title: "Remove " + profileName + "?"
				}, onRemoveConfirm);
			}
			function onRemoveConfirm(err, results) {
				if (err) core.warn("Error asking confirmation", err);
				core.config.deleteProfile(profileName);
				core.config.setProfileKey("Default");
				core.config.loadProfile();
				populateProfileMenu();

				//refreshProfileGeneralList();
				refreshProfileLists();
				core.info("Removed " + profileName + ".");
			}
			
		};
		controlEvents["btnAddFellow"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			return SkunkSchema11.showEditPrompt({
				title: "Player Name",
				value: core.config.character.primaryTie
			}, function onValue(err, results) {
				if (err) return core.warn("Error asking question", err);
				core.config.profile.fellowship.push({
					enabled: true,
					name   : results
				});
				core.config.saveProfile();
				refreshFellowshipList();
			});
		};
		
		controlEvents["lstFellowship"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl);
			var intCol = parseInt(value.split(",")[0], 10);
			var intRow = parseInt(value.split(",")[1], 10);
			var info = shownFellowshipList[intRow - 1];
			
			if (intCol == 0) {
				return SkunkSchema11.getControlPropertyAsync({
					panelName  : viewTitle,
					controlName: szControl,
					property   : "data(" + intCol + "," + intRow + ")"
				}, function onEnabled(err, results) {
					if (err) return core.warn("Error", err);
					info.enabled = results;
					core.config.saveProfile();
					refreshFellowshipList();
				});
			} else if (intCol == 2) {
				return SkunkSchema11.showConfirmationPrompt({
					title: "Remove " + info.name + "?"
				}, function onConfirm(err, results) {
					if (err) return core.warn("Error", err);
					
					var index = core.config.profile.fellowship.indexOf(info);
					if (index >= 0) {
						core.config.profile.fellowship.splice(index, 1);
						core.info("Removed " + info.name + ".");
						core.config.saveProfile();
						refreshFellowshipList();
					}
				});
			}
		};
		controlEvents["btnGiveComps"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			core.showGiveCompsWindow({logger: core.console});
		};
		controlEvents["lstDebug"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl);
			var intCol = parseInt(value.split(",")[0], 10);
			var intRow = parseInt(value.split(",")[1], 10);
			var test = debugTests[intRow];
			test.click();
		};
		controlEvents["btnAddAdvertisement"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			return SkunkSchema11.showEditPrompt({
				title: "Message",
				value: ""
			}, function onValue(err, results) {
				if (err) return core.warn("Error asking question", err);
				core.config.profile.advertisements.push({
					enabled: true,
					text   : results
				});
				core.config.saveProfile();
				refreshAdvertisementsList();
			});
		};
		controlEvents["lstAdvertisements"] = function OnControlEvent(szPanel, szControl, dictSzValue) {
			var value = dictSzValue(szControl);
			var intCol = parseInt(value.split(",")[0], 10);
			var intRow = parseInt(value.split(",")[1], 10);
			var info = shownAdvertisementsList[intRow - 1];
			
			if (intCol == 0) {
				return SkunkSchema11.getControlPropertyAsync({
					panelName  : viewTitle,
					controlName: szControl,
					property   : "data(" + intCol + "," + intRow + ")"
				}, function onEnabled(err, results) {
					if (err) return core.warn("Error", err);
					info.enabled = results;
					core.config.saveProfile();
					refreshAdvertisementsList();
				});
			} else if (intCol == 2) {
				core.info("text: " + info.text);
				return SkunkSchema11.showConfirmationPrompt({
					title: "Remove message?"
				}, function onConfirm(err, results) {
					if (err) return core.warn("Error", err);
					
					var index = core.config.profile.advertisements.indexOf(info);
					if (index >= 0) {
						core.config.profile.advertisements.splice(index, 1);
						core.info("Removed " + info.text + ".");
						core.config.saveProfile();
						refreshAdvertisementsList();
					}
				});
			}
		};
		
		
		
		
		var configProfileList = [];
		configProfileList.push({
			title: "Broadcast requests to fellowship",
			click: function() {
				var self = this;

				//core.config.profile.broadcastRequestsToFellowship
				return SkunkSchema11.showBooleanPrompt({
					title: self.title
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					
					core.config.profile.broadcastRequestsToFellowship = results;
					core.config.saveProfile();

					//refreshProfileGeneralList();
					refreshProfileLists();
				});
			},
			value: function() {
				return core.config.profile.broadcastRequestsToFellowship;
			}
		});
		configProfileList.push({
			title: "Fellowship name",
			click: function() {
				var self = this;
				return SkunkSchema11.showEditPrompt({
					title: "Fellowship name",
					value: core.config.profile.fellowshipName 
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.config.profile.fellowshipName = results;
					core.config.saveProfile();
					refreshProfileLists();
				});
			},
			value: function() {
				return core.config.profile.fellowshipName;
			}
		});

		configProfileList.sort(function(a, b) {
			if (a.title < b.title) return -1;
			if (a.title > b.title) return 1;
			return 0;
		});
		
		function refreshProfileGeneralList() {
			skapi.GetControlProperty(viewTitle, "lstConfigProfileGeneral", "Clear()");
			skapi.GetControlProperty(viewTitle, "lstConfigProfileGeneral", "AddRow()");
			skapi.SetControlProperty(viewTitle, "lstConfigProfileGeneral", "Data(" + 0 + ", " + 0 + ")", "Setting");
			skapi.SetControlProperty(viewTitle, "lstConfigProfileGeneral", "Data(" + 1 + ", " + 0 + ")", "Value");
			configProfileList.forEach(function(info, i) {
				var row = i + 1;
				skapi.GetControlProperty(viewTitle, "lstConfigProfileGeneral", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstConfigProfileGeneral", "Data(" + 0 + ", " + row + ")", info.title);
				skapi.SetControlProperty(viewTitle, "lstConfigProfileGeneral", "Data(" + 1 + ", " + row + ")", String(info.value()));
			});
		}
		
		var shownFellowshipList;
		function refreshFellowshipList() {
			shownFellowshipList = [];
			core.config.profile.fellowship.sort(function(a, b) {
				if (a.name < b.name) return -1;
				if (a.name > b.name) return 1;
				return 0;
			});
			skapi.GetControlProperty(viewTitle, "lstFellowship", "Clear()");
			skapi.GetControlProperty(viewTitle, "lstFellowship", "AddRow()");
			skapi.SetControlProperty(viewTitle, "lstFellowship", "Data(" + 1 + ", " + 0 + ")", "Name");
			var col;
			core.config.profile.fellowship.forEach(function(info, i) {
				col = i + 1;
				skapi.GetControlProperty(viewTitle, "lstFellowship", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstFellowship", "Data(" + 0 + ", " + col + ")", String(info.enabled));
				skapi.SetControlProperty(viewTitle, "lstFellowship", "Data(" + 1 + ", " + col + ")", info.name);
				skapi.SetControlProperty(viewTitle, "lstFellowship", "Data(" + 2 + ", " + col + ", 1)", ICON_START + 4600);
				shownFellowshipList.push(info);
			});
		}

		var shownAdvertisementsList;
		function refreshAdvertisementsList() {
			shownAdvertisementsList = [];
			core.config.profile.advertisements.sort(function(a, b) {
				if (a.text < b.text) return -1;
				if (a.text > b.text) return 1;
				return 0;
			});
			skapi.GetControlProperty(viewTitle, "lstAdvertisements", "Clear()");
			skapi.GetControlProperty(viewTitle, "lstAdvertisements", "AddRow()");
			skapi.SetControlProperty(viewTitle, "lstAdvertisements", "Data(" + 1 + ", " + 0 + ")", "Message");

			var col;
			core.config.profile.advertisements.forEach(function(info, i) {
				col = i + 1;
				skapi.GetControlProperty(viewTitle, "lstAdvertisements", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstAdvertisements", "Data(" + 0 + ", " + col + ")", String(info.enabled));
				skapi.SetControlProperty(viewTitle, "lstAdvertisements", "Data(" + 1 + ", " + col + ")", info.text);
				skapi.SetControlProperty(viewTitle, "lstAdvertisements", "Data(" + 2 + ", " + col + ", 1)", ICON_START + 4600);
				shownAdvertisementsList.push(info);
			});
		}
		
		
		
		
		var configCharacterGeneralList = [];
		configCharacterGeneralList.push({
			title: "Primary Portal Name",
			click: function() {
				var self = this;
				return SkunkSchema11.showEditPrompt({
					title: "Primary portal name",
					value: core.config.character.primaryTie
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.config.character.primaryTie = results;
					core.config.saveCharacter();
					refreshCharacterGeneralList();
				});
			},
			value: function() {
				return core.config.character.primaryTie || "";
			}
		});
		configCharacterGeneralList.push({
			title: "Secondary Portal Name",
			click: function() {
				var self = this;
				return SkunkSchema11.showEditPrompt({
					title: "Secondary portal name",
					value: core.config.character.secondaryTie
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.config.character.secondaryTie = results;
					core.config.saveCharacter();
					refreshCharacterGeneralList();
				});
			},
			value: function() {
				return core.config.character.secondaryTie || "";
			}
		});
		
		configCharacterGeneralList.push({
			title: "Primary Portal Heading",
			click: function() {
				var self = this;
				var heading = core.round(skapi.maplocCur.head);
				return SkunkSchema11.showEditPrompt({
					title: "Primary portal Direction",
					value: heading
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.config.character.primaryTieHeading = results;
					core.config.saveCharacter();
					refreshCharacterGeneralList();
				});
			},
			value: function() {
				return core.config.character.primaryTieHeading || "";
			}
		});
		
		configCharacterGeneralList.push({
			title: "Secondary Portal Heading",
			click: function() {
				var self = this;
				var heading = core.round(skapi.maplocCur.head);
				return SkunkSchema11.showEditPrompt({
					title: "Primary portal Direction",
					value: heading
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.config.character.secondaryTieHeading = results;
					core.config.saveCharacter();
					refreshCharacterGeneralList();
				});
			},
			value: function() {
				return core.config.character.secondaryTieHeading || "";
			}
		});
		
		
		configCharacterGeneralList.sort(function(a, b) {
			if (a.title < b.title) return -1;
			if (a.title > b.title) return 1;
			return 0;
		});
		
		function refreshCharacterGeneralList() {
			skapi.GetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "Clear()");
			skapi.GetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "AddRow()");
			skapi.SetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "Data(" + 0 + ", " + 0 + ")", "Setting");
			skapi.SetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "Data(" + 1 + ", " + 0 + ")", "Value");
			configCharacterGeneralList.forEach(function(info, i) {
				var row = i + 1;
				skapi.GetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "Data(" + 0 + ", " + row + ")", info.title);
				skapi.SetControlProperty(viewTitle, "lstConfigCharactgerGeneral", "Data(" + 1 + ", " + row + ")", String(info.value()));
			});

			//lstConfigCharactgerGeneral
		}
		
		var statusLines = [];
		statusLines.push({
			title: "Uptime",
			value: function() {
				var elapsed = new Date().getTime() - core.status.startTime;
				return core.getElapsedString(elapsed / 1000);
			}
		});
		statusLines.push({
			title: "Buff Requests",
			value: function() {
				return core.status.buffRequests;
			}
		});
		statusLines.push({
			title: "Summon Requests",
			value: function() {
				return core.status.summonRequests;
			}
		});
		statusLines.push({
			title: "Buffs Cast",
			value: function() {
				return core.status.buffsCast;
			}
		});

		statusLines.sort(function(a, b) {
			if (a.title < b.title) return -1;
			if (a.title > b.title) return 1;
			return 0;
		});
		
		function refreshStatusList() {
			skapi.GetControlProperty(viewTitle, "lstStatus", "Clear()");
			statusLines.forEach(function(info, i) {
				skapi.GetControlProperty(viewTitle, "lstStatus", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstStatus", "Data(" + 0 + ", " + i + ")", info.title + ": " + info.value());
			});
		}
		
		core.setInterval(refreshStatusList, 5000);
		
		var listedProfileNames;
		function populateProfileMenu() {
			listedProfileNames = [];
			skapi.GetControlProperty(viewTitle, "chProfile", "Clear()");
			var profiles = core.config.getProfiles();
			var selected;
			profiles.forEach(function(name, i) {
				skapi.GetControlProperty(viewTitle, "chProfile", "AddChoice('" + name + "')");
				if (name == core.config._keyProfile) {
					selected = i;
				}
				listedProfileNames.push(name);
			});
			skapi.SetControlProperty(viewTitle, "chProfile", "selected", selected);
		}
		
		var string = schema.toXML();
		var pretty = SkunkSchema11.prettifyXml(string);
		core.console("pretty: " + pretty);
		skapi.ShowControls(string, false);

		populateProfileMenu();
		
		function refreshProfileLists() {
			refreshBuffsList();
			refreshProfileGeneralList();
			refreshFellowshipList();
			refreshAdvertisementsList();
		}
		
		refreshProfileLists();
		refreshCharacterGeneralList();
		refreshStatusList();
		refreshDebugList();
	};

	core.round = function round(rnum, rlength) { // Arguments: number to core.round, number of decimal places
		if (rlength == null) rlength = 0;
		return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
	};

	core.getInventory = function getInventory() {
		var items = [];
		var acoPack, aco;
		// eslint-disable-next-line no-restricted-syntax
		for (var pack = 0; pack < skapi.cpack; pack++) {
			acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
			// eslint-disable-next-line no-restricted-syntax
			for (var slot = 0; slot < acoPack.citemContents; slot++) {
				aco = skapi.AcoFromIpackIitem(pack, slot);
				items.push(aco);
			}
		}
		return items;
	};

	core.applyMethod = function applyMethod() {
		//core.trace("<applyMethod>");
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};

	
	core.stackInventory = function stackInventory(params, callback) {
		core.debug("<stackInventory>");

		//var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		if (params && params.mcm) {
			acf.mcm = params.mcm;
		}
		var items = core.coacoToArray(acf.CoacoGet()).filter(function _canStack(aco) {
			return aco.citemMaxStack > 1 && aco.citemStack < aco.citemMaxStack;
		});

		items.sort(function(a, b) {
			return a.citemStack - b.citemStack;
		});
		
		return async_js.eachSeries(items, function _each(aco, callback) {
			//core.debug("<stackInventory|_each>");
			if (!aco || !aco.fExists) return core.setImmediate(callback);

			var partners = core.getItemStackPartners({aco: aco, logger: logger, max: aco.citemMaxStack - 1});
			if (partners.length == 0) return core.setImmediate(callback);
			logger("aco: " + aco.szName, aco.citemStack, "partners: " + partners.length);

			partners.sort(function(a, b) {
				return b.citemStack - a.citemStack;
			});

			var acoPartner = partners[0];
			
			logger(aco.szName + " (" + aco.citemStack + "/" + aco.citemMaxStack + ")'s stack partner is " + acoPartner.szName + " (" + acoPartner.citemStack + "/" + acoPartner.citemMaxStack + ")");
			if (!acoPartner || !acoPartner.fExists) return core.setImmediate(callback);

			core.mergeItems({
				aco       : aco,
				acoPartner: acoPartner,
				logger    : logger
			}, function(err, results) {
				if (err) {
					if (err.message == "MUST_PICKUP_FIRST") {
						core.warn(aco.szName + " isn't in our iventory, phantom?");
						core.phantomItems[aco.oid] = true;
						return callback(undefined, true);
					}
					return callback(err);
				}
				callback(undefined, results);
			});
		}, callback);
	};

	core.shouldStackInventory =  function shouldStackInventory(params) {
		core.debug("<shouldStackInventory>");
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		if (params && params.mcm) {
			acf.mcm = params.mcm;
		}
		var items = core.coacoToArray(acf.CoacoGet()).filter(function _canStack(aco) {
			return aco.citemMaxStack > 1 && aco.citemStack < aco.citemMaxStack;
		});
		return items.some(function _hasPartner(aco) {
			var partners = core.getItemStackPartners({aco: aco, logger: logger, max: aco.citemMaxStack - 1});
			return partners.length > 0;
		});
	};

	core.coacoToArray = function coacoToArray(coaco) {
		var arr = [];
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < coaco.Count; i++) {
			arr.push(coaco.Item(i));
		}
		return arr;
	};

	core.getItemStackPartners = function getItemStackPartners(params) {
		//core.debug("<getItemStackPartner>");
		var aco = params.aco;
		var max = (!params || params.max === undefined ? aco.citemMaxStack : params.max);

		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		acf.szName = aco.szName;
		acf.citemStackMax = max;

		var items = core.coacoToArray(acf.CoacoGet());
		
		// Remove aco from available items.
		return items.filter(function(acoPartner) {
			return acoPartner.citemMaxStack > 1 && aco.icon == acoPartner.icon && acoPartner.oid != aco.oid;
		});
	};

	core.stateStack = [];
	

	core.flushStateStack = function flushStateStack() {
		// Clear out the existing stack.
		core.stateStack = core.stateStack.filter(function(session2) {
			//core.emit("onSessionStop", session2);
			core.emit("onSessionDestroy", session2);
			return false;
		});
	};

	core.popState = function popState(session) {
		core.stateStack = core.stateStack.filter(function(session2) {
			if (session == session2) {
				core.emit("onSessionDestroy", session2);
				return false;
			}
			return true;
		});
		if (session == core.currentSession) {
			//core.setImmediate(core.tick);
		}
	};
	
	core.switchState = function switchState(prevSession, newSession) {
		core.popState(prevSession);
		core.pushState(newSession);
	};
	
	core.setState = function setState(session) {
		if (!(session instanceof core.Session)) {
			core.trace("Invalid session");
			throw new core.Error("INVALID_SESSION").setCode(0x4b5ccc98);
		}
		core.flushStateStack();
		core.pushState(session);

		//core.setImmediate(core.tick);
	};
	

	core.pushState = function pushState(session) {
		core.stateStack.forEach(function(session2) {
			if (session2._paused == 0) {
				core.emit("onSessionPause", session2);
				session2._paused = new Date().getTime();
			}
		});
		
		core.stateStack.push(session);

		//core.setImmediate(core.tick);
	};
	
	core.getTopSession = function getTopSession() {
		if (core.stateStack.length > 0) {
			return core.stateStack[ core.stateStack.length - 1];
		}
	};

	core.currentSession = null;
	var tickTimer = null;
	core.startTicking = function startTicking() {
		core.debug("<startTicking>");

		//core.tick();
		//tickTimer = core.setInterval(core.tick, 100);
		
		if (core.config.character.active != true) {
			core.info("Not active anymore, halting ticking");
			return;
		}
		
		if (core.stateStack.length == 0) {
			core.debug("stateStack is empty, adding Start.");
			core.setState(core.createSession({state: "Start"}));
		}

		var session = core.getTopSession();
		
		/*
		if (session == core.currentSession && session._executing == core.lastExecuteTime) { // core.lastExecuteTime
			//core.info("Still executing " + session.state, session._uuid);
			return;
		}
		*/
		
		core.debug("Executing " + session.state + "...", core.currentSession);
		core.currentSession = session;
		
		core.executeTickFunction({
			session: session
		}, function(err, results) {
			//if (err) core.warn("Unhandled state error: ", err);
			if (err) {
				switch(err.message) {
				case "SESSION_PAUSED":
				case "SESSION_DESTOYED":
				//case "STATE_CHANGED":
				//case "CONTAINER_CLOSED":
				//case "CHANGING STATE":
				//case "SWITCHED_STATES":
				//case "HEALTH_SAFER":
					break;
				default:
					core.warn("Error executing " + session.state, err);
				} 
			}
			
			if (typeof session.callback === "function") {
				session.callback(err, results);
			}
			
			if (session != core.getTopSession()) {
				return core.setImmediate(core.startTicking);

				//return core.startTicking();
			}
			
			core.setTimeout(core.startTicking, 100);
		});
	};

	core.tick = function tick() {};
	
	core.lastExecuteTime = 0;
	core.executeTickFunction = function executeTickFunction(params, callback) {
		core.debug("<executeTickFunction>");
		var session = params.session;
		
		function finish() {
			core.debug("<executeTickFunction|finish>");
			if (callback) {
				core.clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;

				core.removeListener("onSessionPause", onSessionPause);
				core.removeListener("onSessionDestroy", onSessionDestroy);
				
				session._executing = 0;
				core.emit("onSessionStop", session);
			}
		}

		if (!session.state || !core.states[session.state]) {
			core.warn("Invalid state", session.state, core.states[session.state]);
			return finish(new core.Error("INVALID_STATE").setCode(0x054fc03a));
		}

		function onSessionPause(session2) {
			core.debug("A session was paused!", (session == session2), session2.state);
			if (session2 == session) {
				//finish(new core.Error("SESSION_PAUSED"));
				core.setImmediate(finish, new core.Error("SESSION_PAUSED").setCode(0x93eafeec));
			}
		}
		core.on("onSessionPause", onSessionPause);
		
		function onSessionDestroy(session2) {
			core.debug("A session was destoryed!", (session == session2), session2.state);
			if (session2 == session) {
				core.setImmediate(finish, new core.Error("SESSION_DESTOYED").setCode(0x48f3b0c4));
			}
		}
		core.on("onSessionDestroy", onSessionDestroy);

		core.emit("onSessionStart", session);
		session._started = session._started || new Date().getTime();
		
		if (session._paused > 0) {
			core.emit("onSessionResume", session);
			session._paused = 0;
		}

		var executeTime = session._executing = core.lastExecutionTime = new Date().getTime();
		core.states[session.state]({
			session    : session,
			executeTime: executeTime
		}, finish);

		var tid = core.setTimeout(core.timedOut, 1000 * 60 * 5, finish, "EXECUTE_TICK_TIMED_OUT");
	};

	core.timedOut = function timedOut(resolve) {
		resolve(new core.Error("TIMED_OUT").setCode(0x6fcfe6c5));
	};

	var simpleUUID = core.simpleUUID = function simpleUUID() {
		// eslint-disable-next-line no-magic-numbers
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
			.substring(1);
	};

	var Session = core.Session = function Session(params) {
		if (!(this instanceof Session)) {
			return new Session(params);
		}

		//EventEmitter.apply(this);
		for (var key in params) {
			if (!params.hasOwnProperty(key)) continue;
			this[key] = params[key];
		}
		this._uuid = core.simpleUUID(); //core.generateUUID();
		this._paused = 0;
		this._started = 0;
		this._executing = 0;
	};
	
	Session.prototype.toString = function () {
		return "[Session " + this.state + "|" + this._uuid + "]";
	};

	core.createSession = function createSession(params) {
		var session = new Session(params);
		core.emit("onSessionCreate", session);
		return session;
	};

	core.getMyBuffs = function getMyBuffs() {
		var layers = {};
		var spell;
		for (var i = 0; i < skapi.cospell.Count; i++) {
			spell = skapi.cospell.Item(i);

			//spells.push(spell);
			layers[spell.spellid] = layers[spell.spellid] || [];
			layers[spell.spellid].push(spell);
		}
		var spells = [];
		Object.keys(layers).forEach(function(spellid) {
			layers[spellid].sort(function(a, b) {
				return b.csecRemain - a.csecRemain;
			});
			spells.push(layers[spellid][0]);
		});
		return spells;
	};

	core.doCastSpell = function doCastSpell(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var spellid = params.spellid;
		var aco = params.aco;
		
		
		if (core.fCombatMode != true) {
			return core.setCombatMode({mode: true}, function onCombat(err, results) {
				if (err) return callback(err);
				return core.doCastSpell(params, callback);
			});
		}

		return core.castSpell({spellid: spellid, aco: aco}, function onCast(err, results) {
			if (err) {
				core.warn("Error casting spell", err);
				return callback(err);
			}

			//core.popState(session, undefined, true);
			callback(undefined, true);
		});
	};

	var skinfoHealth = skapi.SkinfoFromVital(vitalHealthMax);	
	core.getHealthPercentage = function getHealthPercentage() {
		//return core.healthCur && (core.healthCur / skinfoHealth.lvlCur) || 1;
		return skapi.healthCur && (skapi.healthCur / skinfoHealth.lvlCur) || 1;
	};

	var skinfoStamina = skapi.SkinfoFromVital(vitalStaminaMax);	
	core.getStaminaPercentage = function getStaminaPercentage() {
		//return core.staminaCur && (core.staminaCur / skinfoStamina.lvlCur) || 1;
		return skapi.staminaCur && (skapi.staminaCur / skinfoStamina.lvlCur) || 1;
	};
	
	var skinfoMana = skapi.SkinfoFromVital(vitalManaMax);	
	core.getManaPercentage = function getManaPercentage() {
		//return core.manaCur && (core.manaCur / skinfoMana.lvlCur) || 1;
		return skapi.manaCur && (skapi.manaCur / skinfoMana.lvlCur) || 1;
	};

	core.toHex = function toHex(a) {
		if (!a) {
			a = 0;
		}
		return (a < 0 ? 4294967296 + a : a).toString(16).toUpperCase();
	};

	core.headingTurn = function headingTurn(params) { //intendedHeading, currentHeading
		//if (typeof currentHeading === "undefined") currentHeading = skapi.maplocCur.head;
		var heading = params.heading;
		var current = params.current || skapi.maplocCur.head;
		var heading = ((heading - current) % 360);
		while (heading < 0) {
			heading += 360; // % in jscript doesn't touch negative numbers...
		}
		if (heading > 180) {
			heading -= 360;
		}
		return heading;
	};

	core.face = function face(params, callback) {
		var heading = params.heading;
		var variance = params.variance || 15;
		var timeout = params.timeout || 5000;
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		logger("<face>", "heading: " + heading, "variance: " + variance);
		
		function finish(err, result) {
			core.debug("<face|finish>", err, result);
			if (tid) clearTimeout(tid);
			if (tid2) clearTimeout(tid2);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
		}

		var rHeading = core.headingTurn({heading: heading});
		
		logger("rHeading1: " + rHeading);
		
		if (Math.abs(rHeading) <= variance) {
			return finish(undefined, true);
		}

		function tick() {
			rHeading = core.headingTurn({heading: heading});
			logger("rHeading2: " + rHeading, "variance: " + variance, (Math.abs(rHeading) <= variance));
			if (Math.abs(rHeading) <= variance) {
				return finish(undefined, true);
			}
		}

		skapi.TurnToHead(heading);
		
		// skapi.TurnToHead seems to turn on walk mode, slowing the turn. Lets turn it back off.
		if (skapi.chop & chopRunAsDefault) {
			skapi.Keys(cmidMovementWalkMode, kmoUp);
		}
		
		var tid2 = core.setInterval(tick, 1);
		var tid = core.setTimeout(core.timedOut, timeout, finish, "FACE_TIME_OUT");
	};

	core.createFellowship = function createFellowship(params, callback) {
		//var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
		var name = (!params || params.name === undefined ? "Fellowship" : params.name);
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		var handler = {};
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			if (szMsg.match(/You have created the Fellowship of (.*)./i)) {
				SkunkLayout10.openFellowshipPanel();
				return finish(undefined, true);
			}
			core.warn("<createFellowship|OnChatBoxMessage> UNCAUGHT ", szMsg, cmc);
		};
		core.addHandler(evidOnChatBoxMessage,            handler);

		SkunkLayout10.createFellowship(name);
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 2000, finish, "CREATE_FELLOWSHIP_TIMED_OUT");
	};

	core.openFellowship = function openFellowship(params, callback) {
		
		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			if (szMsg.match(/Fellowship is now open/i)) {
				SkunkLayout10.openFellowshipPanel();
				return finish(undefined, true);
			} else if (szMsg.match(/is now an open fellowship/i)) {
				SkunkLayout10.openFellowshipPanel();
				return finish(undefined, true);
			} else if (szMsg.match(/Fellowship is now closed/i)) {
				return finish(new core.Error("FELLOWSHIP_CLOSED").setCode(0xc7551d60));
			}
			core.warn("<openFellowship|OnChatBoxMessag> UNCAUGHT ", szMsg, cmc);
		};
		core.addHandler(evidOnChatBoxMessage,            handler);
		
		SkunkLayout10.openFellowshipPanel();
		skapi.ActivateAC();
		SkunkLayout10.fellowshipOpen.click();
		
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 3000, finish, "OPEN_FELLOWSHIP_TIMED_OUT");
	};

	core.cloneFellow = function cloneFellow(fellow) {
		return {
			oid       : fellow.oid,
			szName    : fellow.szName,
			lvl       : fellow.lvl,
			fShareLoot: fellow.fShareLoot,
			healthMax : fellow.healthMax,
			staminaMax: fellow.staminaMax,
			manaMax   : fellow.manaMax,
			healthCur : fellow.healthCur,
			staminaCur: fellow.staminaCur,
			manaCur   : fellow.manaCur
		};
	};

	// TODO: Switch this to async, with it opening the fellowship panel.
	core.getFellowshipSkapi = function getFellowshipSkapi() {
		return core.coacoToArray(skapi.cofellow).map(core.cloneFellow);
	};

	core.getPlayers = function factory() {
		function _notMe(aco) {
			return aco != skapi.acoChar;
		}
		return function getPlayers() {
			var acf = skapi.AcfNew();
			acf.oty = otyPlayer;
			return core.coacoToArray(acf.CoacoGet()).filter(_notMe);
		};
	}();
	
	core.isOnOurFellowList = function isOnOurFellowList(params) {
		var name = params.name;
		return core.config.profile.fellowship.some(function(info) {
			return info.enabled == true && name == info.name;
		});
	};
	
	core.getFellowship = function getFellowship(params, callback) {
		function finish() {
			core.clearTimeout(tid);
			FellowshipFilter10.removeCallback(onFellowship);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		function onFellowship(payload) {
			return finish(undefined, payload);
		}
		FellowshipFilter10.addCallback(onFellowship);

		skapi.ActivateAC();
		SkunkLayout10.examineCloseButton.click();
		SkunkLayout10.openFellowshipPanel();
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 1000 * 2, finish, "GET_FELLOWSHIP_TIMED_OUT");
	};
	
	core.requestCommandResult = function requestCommandResult(params, callback) {
		var name = params.name;
		var command = params.command;
		
		var regex = new RegExp("// " + command + ": (.*)", "i");
		
		function finish() {
			core.clearTimeout(tid);

			//core.removeHandler(evidNil, handler);
			SkunkComm.emitter.removeListener("onMessage", onMessage);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		function onMessage(params) {
			if (params.evid != 34) return;
			if (params.szSender != name) return;
			if (params.szMsg.match(regex)) {
				var result = RegExp.$1;
				return finish(undefined, result);
			}
			core.debug("<requestCommandResult|onMessage> uncaught", JSON.stringify(params));
		}
		SkunkComm.emitter.on("onMessage", onMessage);

		SkunkComm.tell({
			szRecipient: name, 
			szMsg      : command
		});
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 1000 * 5, finish, "REQUEST_COMMAND_RESULT_TIMED_OUT");
	};
	
	core.recruitPlayer = function recruitPlayer(params, callback) {
		var aco = params.aco;
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			if (szMsg.match(/(.*) is now a member of your Fellowship./i)) {
				var name = RegExp.$1;
				if (name == aco.szName) {
					return finish(undefined, true);
				}
			}
			core.warn("<recruitPlayer|OnChatBoxMessage> UNCAUGHT ", szMsg, cmc);
		};
		core.addHandler(evidOnChatBoxMessage,            handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY").setCode(0x0dfd6ade));
			} else if (szMsg.match(/That person is already in your fellowship/)) {
				return finish(new core.Error("ALREADY_RECRUITED").setCode(0xedbd1dcc));
			}
			core.warn("<recruitPlayer|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,            handler);

		SkunkLayout10.openFellowshipPanel();

		//skapi.ActivateAC();
		skapi.SelectAco(aco);
		SkunkLayout10.fellowshipRecruit.click();
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 2000, finish, "RECRUIT_PLAYER_TIMED_OUT");
	};

	core.quitFellowship = function quitFellowship(params, callback) {
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		var handler = {};
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			if (szMsg.match(/You are no longer a member of the (.*) Fellowship./i)) {
				return finish(undefined, true);
			}
			core.warn("<quitFellowship|OnChatBoxMessage> UNCAUGHT ", szMsg, cmc);
		};
		core.addHandler(evidOnChatBoxMessage,            handler);
		
		SkunkLayout10.openFellowshipPanel();
		skapi.ActivateAC();
		SkunkLayout10.fellowshipQuit.click();
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 2000, finish, "QUIT_FELLOWSHIP_TIMED_OUT");
	};

	core.waitForFellowshipRecruit = function waitForFellowshipRecruit(params, callback) {
		core.debug("<waitForFellowshipRecruit>");

		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		var handler = {};
		handler.OnFellowCreate = function OnFellowCreate(szName, fShareExp, oidLeader, cofellow) {
			return finish(undefined, true);
		};
		core.addHandler(evidOnFellowCreate, handler);

		var tid = core.setTimeout(core.timedOut, params && params.timeout || 1000 * 10, finish, "REQUEST_RECRUIT_TIMED_OUT");
	};

	core.loadUILayout = function loadUILayout() {
		var uuid = core.simpleUUID();
		skapi.InvokeChatParser("/saveui " + uuid);
		if (SkunkLayout10.exists(uuid)) {
			SkunkLayout10.loadUI(uuid);
			SkunkLayout10.deleteUI(uuid);
		}
		core.warn("Loaded client UI layout, don't move panel positions.");
	};

	core.getElapsedString = function getElapsedString(seconds, precision) {
		// eslint-disable-next-line no-magic-numbers
		var numdays = Math.floor((seconds % 31536000) / 86400); 
		// eslint-disable-next-line no-magic-numbers
		var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
		// eslint-disable-next-line no-magic-numbers
		var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
		// eslint-disable-next-line no-magic-numbers
		var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;

		numseconds = Math.max(numseconds, 1);
		
		var parts = 0;
		
		var string = ""; //core.round(numseconds) + "s";

		if (numdays > 0) {
			string = numdays + "d";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}
		
		if (numhours > 0) {
			string += " " + numhours + "h";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}
		
		if (numminutes > 0) {
			string += " " + numminutes + "m";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}

		if (numseconds > 0) {
			string += " " + core.round(numseconds) + "s";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}
		
		return core.trimStr(string);
	};

	core.trimStr = function trimStr(strInput) {
		var objRegex = new RegExp("(^\\s+)|(\\s+$)");
		return strInput.replace(objRegex, "");
	};

	core.showGiveCompsWindow = function showGiveCompsWindow(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var title = "Give Comps";
		var viewWidth = 200;
		var viewHeight = 200;
		
		var Element = SkunkSchema11.Element;
		var schema = Element("view", {title: title, icon: 1234, width: viewWidth, height: viewHeight + 32}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 0,                 text: "Copper Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 0,     left: 100,  text: 0, name: "edCopperScarab",    imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 20,                text: "Gold Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 20,    left: 100,  text: 0, name: "edGoldScarab",      imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 40,                text: "Iron Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 40,    left: 100,  text: 0, name: "edIronScarab",      imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 60,                text: "Lead Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 60,    left: 100,	text: 0, name: "edLeadScarab",      imageportalsrc: 4726}),

				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 80,                text: "Mana Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 80,    left: 100,	text: 0, name: "edManaScarab",  imageportalsrc: 4726}),

				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 100,               text: "Platinum Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 100,    left: 100,	text: 0, name: "edPlatinumScarab",  imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 120,               text: "Pyreal Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 120,   left: 100,	text: 0, name: "edPyrealScarab",    imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 140,               text: "Silver Scarab: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 140,   left: 100,	text: 0, name: "edSilverScarab",    imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.StaticText', width: 100, height: 20, top: 160,               text: "Prismatic Taper: "}),
				Element("control", {progid: 'DecalControls.Edit',       width: 100, height: 20, top: 160,   left: 100,	text: 0, name: "edPrismaticTaper",  imageportalsrc: 4726}),
				
				Element("control", {progid: 'DecalControls.PushButton', name: "btnRun",     width: 40, height: 20, top: viewHeight - 20, left: 0, text: "Run"}),
				
				Element("control", {progid: 'DecalControls.PushButton', name: "btnQuit",     width: 40, height: 20, top: viewHeight - 20, left: viewWidth - 40, text: "Quit"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnQuit") {
				return finish();
			} else if (szControl == "btnRun") {
				if (!skapi.acoSelected) return core.warn("Select something first.");

				var acoTarget = skapi.acoSelected;
				
				var compAmounts;
				return async_js.parallel({
					"Copper Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName  : title,controlName: "edCopperScarab",property   : "text"}, callback);
					},
					"Gold Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName  : title,controlName: "edGoldScarab",property   : "text"}, callback);
					},
					"Iron Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName  : title,controlName: "edIronScarab",property   : "text"}, callback);
					},
					"Lead Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName: title, controlName: "edLeadScarab", property: "text"}, callback);
					},
					"Mana Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName: title, controlName: "edManaScarab", property: "text"}, callback);
					},
					"Platinum Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName: title, controlName: "edPlatinumScarab", property: "text"}, callback);
					},
					"Pyreal Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName: title, controlName: "edPyrealScarab", property: "text"}, callback);
					},
					"Silver Scarab": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName: title, controlName: "edSilverScarab", property: "text"}, callback);
					},
					"Prismatic Taper": function(callback) {
						return SkunkSchema11.getControlPropertyAsync({panelName: title, controlName: "edPrismaticTaper", property: "text"}, callback);
					}
				}, function onLocationValues(err, results) {
					if (err) return core.warn("Error", err);

					Object.keys(results).forEach(function(key) {
						results[key] = Number(results[key]);
					});
					compAmounts = results;
					//logger("results: " + JSON.stringify(results));
					
					core.requestCommandResult({name: acoTarget.szName, command: "sb comps"}, onComps);
				});

				function onComps(err, json) {
					if (err) return core.warn("Error asking for comps", err);
					
					var data;
					try {
						data = JSON.parse(json);
					} catch (err) {
						core.warn("Error parsing json", err);
						return;
					}
					
					var itemsToGive = [];
					Object.keys(data).forEach(function(key) {
						var amount = Math.max(0, compAmounts[key] - data[key]);
						if (amount > 0) {
							//giveAmounts[key] = amount;
							itemsToGive.push({
								itemName: key, 
								amount: amount
							});
						}
					});
					
					//logger("itemsToGive: " + JSON.stringify(itemsToGive));
					
					return async_js.eachSeries(itemsToGive, eachItem, onGave);
				}
				
				function eachItem(info, callback) {
					//core.giveToAco({acoItem: aco, acoTarget: acoTarget}, callback);
					var itemName = info.itemName;
					var amount = info.amount;
					
					var acf = skapi.AcfNew();
					acf.olc = olcInventory;
					acf.szName = itemName;
					var items = core.coacoToArray(acf.CoacoGet());
					
					if (items.length == 0) {
						core.warn("Out of " + itemName + "!");
						return callback();
					}
					
					return core.giveAmountOfItem({acoTarget: acoTarget, items: items, amount: amount}, callback);
				}
				
				function onGave(err, results) {
					if (err) return core.warn("Error giving comps", err);
					core.info("Done");
				}
			}
			core.info("<OnControlEvent>", szPanel, szControl, dictSzValue(szControl));
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		
		var string = schema.toXML();
		var pretty = SkunkSchema11.prettifyXml(string);
		core.console("pretty: " + pretty);
		skapi.ShowControls(string, true);
	};

	core.giveAmountOfItem = function giveAmountOfItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		var acoTarget = params.acoTarget;
		
		var items = params.items;
		var amount = params.amount;
		
		var remaining = amount;
		
		return async_js.eachSeries(items, function each(aco, callback) {
			if (remaining <= 0) return callback();
			if (aco.citemStack <= remaining) {
				remaining -= aco.citemStack;
				return core.giveToAco({acoItem: aco, acoTarget: acoTarget}, callback);
			}

			return core.splitAco({
				aco     : aco,
				quantity: Math.min(remaining, aco.citemStack)
			}, function onSplit(err, acoSplit) {
				if (err) return callback(err);
				remaining -= acoSplit.citemStack;
				return core.giveToAco({acoItem: acoSplit, acoTarget: acoTarget}, callback);
			});
		}, callback);
	};

	core.saveJsonFile = function saveJsonFile( params ) {
		var path = params.path;
		var data = params.data;
		var json = JSON.stringify(data, null, "\t");
		if (!core.existsSync(params.path)) {
			core.touchSync(params.path);
		}
		core.writeFileSync(params.path, json);
	};

	core.getJsonFile = function getJsonFile( params ) {
		core.console("Exists: " + core.existsSync(params.path));
		if (core.existsSync(params.path)) {
			var json = core.readFileSync(params.path);
			var items;
			try {
				items = JSON.parse(json);
			} catch (e) {
				return;
			}
			return items;
		}
	};

	core.Error = function CustomError(message) {
		var err = Error.apply(this, arguments);
		this.stack = err.stack;
		this.name = err.name || "NO_NAME";
		this.message = err.message;
		this.description = err.description;
		core.trace("Error occurred: " + message);
	};
	core.Error.prototype = new Error();

	//core.Error.prototype = Error.prototype; //new Error();
	core.Error.prototype.name = core.Error.name;
	core.Error.prototype.constructor = core.Error;
	core.Error.prototype.set = function set(key, value) {
		this[key] = value;
		return this;
	};
	
	core.Error.prototype.setCode = function setCode(value) {
		this.code = value;
		return this;
	};
	
	core.padLeft = function padLeft(szString, maxSize, padder) {
		padder = padder || " ";
		if (szString.length < maxSize) {
			szString = Array(1 + (maxSize - szString.length)).join(padder) + szString;
		}
		return szString;
	};

	Error.prototype.toString = function toString() {
		if (this.code) {
			return this.name + ": " + this.message + " [0x" + core.padLeft(core.toHex(this.code), 8, "0") + "]";
		}
		return this.name + ": " + this.message;
	};

	core.getInventorySize = function getInventorySize() {
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		return core.coacoToArray(acf.CoacoGet()).length;
	};

	return core;
}));

if (typeof SkunkBuffer10 !== "undefined") {
	// eslint-disable-next-line no-undef
	main = SkunkBuffer10.main;
}