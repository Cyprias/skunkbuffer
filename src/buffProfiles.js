var core = SkunkBuffer10;
var buffProfiles = core.buffProfiles = {};

buffProfiles["strength"]            = ["Strength Other"];
buffProfiles["endurance"]           = ["Endurance Other"];
buffProfiles["coordination"]        = ["Coordination Other"];
buffProfiles["quickness"]           = ["Quickness Other"];
buffProfiles["focus"]               = ["Focus Other"];
buffProfiles["willpower"]           = ["Willpower Other"];
buffProfiles["weapon"]              = ["Aura of Blood Drinker Other", "Aura of Defender Other", "Aura of Heart Seeker Other", "Aura of Swift Killer Other"];
buffProfiles["wand"]                = ["Aura of Spirit Drinker Other", "Aura of Hermetic Link Other", "Aura of Defender Other"];
buffProfiles["alchemy"]             = ["Alchemy Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);
buffProfiles["arcane"]              = ["Arcane Enlightenment Other"].concat(buffProfiles["focus"]);
buffProfiles["cooking"]             = ["Cooking Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);
buffProfiles["creature"]            = ["Creature Enchantment Mastery Other"].concat(buffProfiles["focus"], buffProfiles["willpower"]);
buffProfiles["deception"]           = ["Deception Mastery Other"];
buffProfiles["finessew"]            = ["Finesse Weapon Mastery Other"].concat(buffProfiles["quickness"], buffProfiles["coordination"]);
buffProfiles["fletching"]           = ["Fletching Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);
buffProfiles["healing"]             = ["Healing Mastery Other"].concat(buffProfiles["focus"], buffProfiles["coordination"]);
buffProfiles["heavyw"]              = ["Heavy Weapon Mastery Other"].concat(buffProfiles["strength"], buffProfiles["coordination"]);
buffProfiles["item"]                = ["Item Enchantment Mastery Other"].concat(buffProfiles["focus"], buffProfiles["willpower"], buffProfiles["wand"]);
buffProfiles["jump"]                = ["Jumping Mastery Other"].concat(buffProfiles["strength"], buffProfiles["coordination"]);
buffProfiles["leadership"]          = ["Leadership Mastery Other"];
buffProfiles["life"]                = ["Life Magic Mastery Other"].concat(buffProfiles["focus"], buffProfiles["willpower"]);
buffProfiles["Lightw"]              = ["Light Weapon Mastery Other"].concat(buffProfiles["strength"], buffProfiles["coordination"]);
buffProfiles["lockpick"]            = ["Lockpick Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);
buffProfiles["loyalty"]             = ["Fealty Other"];
buffProfiles["magicd"]              = ["Magic Resistance Other"].concat(buffProfiles["focus"], buffProfiles["willpower"]);
buffProfiles["manac"]               = ["Mana Conversion Mastery Other"].concat(buffProfiles["focus"], buffProfiles["willpower"]);
buffProfiles["meleed"]              = ["Invulnerability Other"].concat(buffProfiles["quickness"], buffProfiles["coordination"]);
buffProfiles["missiled"]            = ["Impregnability Other"].concat(buffProfiles["quickness"], buffProfiles["coordination"]);
buffProfiles["missilew"]            = ["Missile Weapon Mastery Other"].concat(buffProfiles["coordination"]);
buffProfiles["run"]                 = ["Sprint Other"].concat(buffProfiles["quickness"]);
buffProfiles["war"]                 = ["War Magic Mastery Other"].concat(buffProfiles["focus"], buffProfiles["willpower"]);
buffProfiles["voidm"]               = ["Void Magic Mastery Other"].concat(buffProfiles["focus"], buffProfiles["willpower"]);
buffProfiles["twohandw"]            = ["Two Handed Combat Mastery Other"].concat(buffProfiles["strength"], buffProfiles["coordination"]);
buffProfiles["dualwield"]           = ["Dual Wield Mastery Other"].concat(buffProfiles["coordination"]);
buffProfiles["salvaging"]           = ["Arcanum Enlightenment"];
buffProfiles["armortink"]           = ["Armor Tinkering Expertise Other"].concat(buffProfiles["focus"], buffProfiles["endurance"]);
buffProfiles["itemtink"]            = ["Item Tinkering Expertise Other"].concat(buffProfiles["focus"], buffProfiles["coordination"]);
buffProfiles["magictink"]           = ["Magic Item Tinkering Expertise Other"].concat(buffProfiles["focus"]);
buffProfiles["weapontink"]          = ["Weapon Tinkering Expertise Other"].concat(buffProfiles["focus"], buffProfiles["strength"]);

buffProfiles["tink"]                = Array.prototype.concat(
	buffProfiles["armortink"],
	buffProfiles["itemtink"],
	buffProfiles["magictink"],
	buffProfiles["weapontink"],
	buffProfiles["salvaging"]
);

buffProfiles["alchemy"]             = ["Alchemy Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);
buffProfiles["cooking"]             = ["Cooking Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);
buffProfiles["fletching"]           = ["Fletching Mastery Other"].concat(buffProfiles["coordination"], buffProfiles["focus"]);

buffProfiles["trades"]              = Array.prototype.concat(
	buffProfiles["alchemy"],
	buffProfiles["cooking"],
	buffProfiles["fletching"]
);

buffProfiles["protects"]            = ["Armor Other", "Acid Protection Other", "Blade Protection Other", "Bludgeoning Protection Other", "Cold Protection Other", "Fire Protection Other", "Lightning Protection Other", "Piercing Protection Other"];
buffProfiles["regens"]              = ["Regeneration Other", "Rejuvenation Other", "Mana Renewal Other"].concat(buffProfiles["endurance"], buffProfiles["willpower"]);
buffProfiles["banes"]               = ["Impenetrability", "Acid Bane", "Blade Bane", "Bludgeon Bane", "Flame Bane", "Frost Bane", "Lightning Bane", "Piercing Bane"];

buffProfiles["xpchain"]             = Array.prototype.concat(
	buffProfiles["loyalty"],
	buffProfiles["leadership"]
);

buffProfiles["mule"]                = Array.prototype.concat(
	buffProfiles["strength"],
	buffProfiles["run"]
);

buffProfiles["3school"]             = Array.prototype.concat(
	buffProfiles["manac"],
	buffProfiles["creature"],
	buffProfiles["item"],
	buffProfiles["life"],
	buffProfiles["regens"]
);

buffProfiles["combat"]              = Array.prototype.concat(
	buffProfiles["item"],
	buffProfiles["manac"],
	buffProfiles["endurance"], 
	buffProfiles["arcane"], 
	buffProfiles["jump"],
	buffProfiles["magicd"],
	buffProfiles["meleed"],
	buffProfiles["missiled"],
	buffProfiles["run"],
	buffProfiles["protects"],
	buffProfiles["regens"],
	buffProfiles["xpchain"],
	buffProfiles["salvaging"]

//	buffProfiles["banes"]
);

buffProfiles["mage"]              = Array.prototype.concat(
	buffProfiles["life"],
	buffProfiles["combat"],
	buffProfiles["creature"],
	buffProfiles["war"],
	buffProfiles["voidm"]
);

buffProfiles["void"]              = Array.prototype.concat(
	buffProfiles["life"],
	buffProfiles["combat"],
	buffProfiles["creature"],
	buffProfiles["voidm"]
);

buffProfiles["heavy"]              = Array.prototype.concat(
	buffProfiles["combat"],
	buffProfiles["heavyw"],
	buffProfiles["healing"],

	////buffProfiles["shield"],
	buffProfiles["dualwield"],
	buffProfiles["weapon"]
);

buffProfiles["light"]              = Array.prototype.concat(
	buffProfiles["combat"],
	buffProfiles["Lightw"],
	buffProfiles["healing"],

	////buffProfiles["shield"],
	buffProfiles["dualwield"],
	buffProfiles["weapon"]
);

buffProfiles["finesse"]              = Array.prototype.concat(
	buffProfiles["combat"],
	buffProfiles["finessew"],
	buffProfiles["healing"],

	////buffProfiles["shield"],
	buffProfiles["dualwield"],
	buffProfiles["weapon"]
);

buffProfiles["missile"]              = Array.prototype.concat(
	buffProfiles["combat"],
	buffProfiles["missilew"],
	buffProfiles["healing"],
	buffProfiles["fletching"],
	buffProfiles["weapon"]
);

buffProfiles["twohand"]              = Array.prototype.concat(
	buffProfiles["combat"],
	buffProfiles["twohandw"],
	buffProfiles["healing"],
	buffProfiles["weapon"]
);

// alises
buffProfiles["lore"]        = buffProfiles["arcane"];
buffProfiles["bow"]         = buffProfiles["missile"];
buffProfiles["xbow"]        = buffProfiles["missile"];
buffProfiles["xp"]          = buffProfiles["xpchain"];
buffProfiles["prots"]       = buffProfiles["protects"];
buffProfiles["lp"]          = buffProfiles["lockpick"];
buffProfiles["salvage"]     = buffProfiles["salvaging"];
buffProfiles["cook"]        = buffProfiles["cooking"];
buffProfiles["fletch"]      = buffProfiles["fletching"];
buffProfiles["trade"]       = buffProfiles["trades"];
buffProfiles["lw"]          = buffProfiles["light"];
buffProfiles["hw"]          = buffProfiles["heavy"];
buffProfiles["fw"]          = buffProfiles["finesse"];
buffProfiles["mw"]          = buffProfiles["missile"];
buffProfiles["trademule"]   = buffProfiles["trades"];
buffProfiles["craft"]       = buffProfiles["trades"];
buffProfiles["tinker"]      = buffProfiles["tink"];
buffProfiles["tinkers"]     = buffProfiles["tink"];
buffProfiles["tinks"]       = buffProfiles["tink"];
buffProfiles["fin"]         = buffProfiles["finesse"];
buffProfiles["leader"]      = buffProfiles["leadership"];
buffProfiles["archer"]      = buffProfiles["missile"];
buffProfiles["2h"]          = buffProfiles["twohand"];
buffProfiles["2hand"]       = buffProfiles["twohand"];
buffProfiles["two"]         = buffProfiles["twohand"];
buffProfiles["dual"]        = buffProfiles["dualwield"];
buffProfiles["shield"]      = buffProfiles["banes"];

//skapi.OutputLine("buffProfiles loaded", opmConsole);