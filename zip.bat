@echo off
set /p version="Version: "

"C:\Program Files\7-Zip\7z.exe" a ../SkunkBuffer_%version%.zip ../SkunkBuffer/ -xr!.vs -xr!.git -xr!logs -xr!zip.bat -xr!.gitmodules -xr!.gitignore -x!SkunkBuffer/configs/*