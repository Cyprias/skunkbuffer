/********************************************************************************************\
	File Name:      FellowshipFilter-1.0.js
	Purpose:        
	Creator:        Cyprias
	Date:           04/14/2020
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "FellowshipFilter-1.0";
var MINOR = 181020;

(function (factory) {
	var params = {};
    if (typeof module === 'object' && module.exports) {
        module.exports = factory(params);
	} else {
        FellowshipFilter10 = factory(params);
	}
}(function (params) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.debugging = true;

	var mtyFellowshipUpdate                = 0xF7B002BE - 0x100000000;

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;

	/******************************************************************************\
		debug: Print a message to chat.
	\******************************************************************************/
	core.debug = function debug(szMsg) {
		if (core.debugging == true) {
			skapi.OutputSz(core.MAJOR + " " + szMsg + "\n", opmConsole);
		}
	};

	function printTypesAtPosition(mbuf, position) {
		mbuf.ResetRead()
		mbuf.SkipCb(position); 
	
		var BYTE = mbuf.Get_BYTE();
		core.debug("Get_BYTE: " + BYTE);
		
		mbuf.ResetRead()
		mbuf.SkipCb(position); 
		var WORD = mbuf.Get_WORD();
		core.debug("Get_WORD: " + WORD);

		mbuf.ResetRead()
		mbuf.SkipCb(position); 
		var DWORD = mbuf.Get_DWORD();
		core.debug("Get_DWORD: " + DWORD);
		
		mbuf.ResetRead()
		mbuf.SkipCb(position); 
		var f = mbuf.Get_float();
		core.debug("Get_float: " + f);

		mbuf.ResetRead()
		mbuf.SkipCb(position); 
		var QWORD = mbuf.Get_QWORD();
		core.debug("Get_QWORD: " + QWORD);

		mbuf.ResetRead()
		mbuf.SkipCb(position); 
		var d = mbuf.Get_double();
		core.debug("Get_double: " + d);
		
		
		mbuf.ResetRead()
		mbuf.SkipCb(position);
		var String;
		try 
		{
			String = mbuf.Get_String();
			core.debug("Get_String: " + String);
		}
		catch (e) 
		{}
	}

	var handler = {};
	/******************************************************************************\
		fOnRawServerMessage: Process raw server messages.
	\******************************************************************************/
	handler.OnRawServerMessage = function fOnRawServerMessage(mty, mbuf) {
		switch(mty) { // Handle the types we define
		case mtyFellowshipUpdate:
			mbuf.SkipCb(4);
			
			core.debug("Fellowship update!");
			
			var payload = {};
			payload.object = mbuf.Get_DWORD();
			payload.sequence = mbuf.Get_DWORD();
			payload.event = mbuf.Get_DWORD();
			if (payload.event == 0x2BE) {
				payload.count = mbuf.Get_WORD();
				payload.tableSize = mbuf.Get_WORD();

				payload.fellows = [];
				for (var i=0; i < payload.count; i++) {
					var fellow = {};
					fellow.object = mbuf.Get_DWORD();
					fellow.cpCached = mbuf.Get_DWORD();
					fellow.lumCached = mbuf.Get_DWORD();
					fellow.level = mbuf.Get_DWORD();
					fellow.maxHealth = mbuf.Get_DWORD();
					fellow.maxStam = mbuf.Get_DWORD();
					fellow.maxMana = mbuf.Get_DWORD();
					fellow.curHealth = mbuf.Get_DWORD();
					fellow.curStam = mbuf.Get_DWORD();
					fellow.curMana = mbuf.Get_DWORD();
					fellow.shareLoot = mbuf.Get_DWORD();
					fellow.name = mbuf.Get_String();
					payload.fellows.push(fellow);
				}
				
				payload.name = mbuf.Get_String();
				payload.leader = mbuf.Get_DWORD();
				payload.shareXp = mbuf.Get_DWORD();
				payload.evenXpSplit = mbuf.Get_DWORD();
				payload.openFellow = mbuf.Get_DWORD();
				payload.locked = mbuf.Get_DWORD();
				
				/*
				payload.count2 = mbuf.Get_WORD();
				core.debug("payload: " + JSON.stringify(payload, undefined, "\t"));
				mbuf.SkipCb(4);
				var complete = (mbuf.ibInMsg / mbuf.cbOfMsg) * 100;
				core.debug("complete: " + complete, opmChatWnd);
				printTypesAtPosition(mbuf, mbuf.ibInMsg)
				*/
				
				fireCallbacks(payload);
			}
			break;
		}
	};

	var _callbacks = [];
	/******************************************************************************\
		addCallback: Add a evid callback.
	\******************************************************************************/
	core.addCallback = function addCallback(method, thisArg) {
		_callbacks.push({method:method, thisArg:thisArg});
		if (_callbacks.length == 1) {
			skapi.AddHandler(mtyFellowshipUpdate,  handler);
		}
	};
	
	/******************************************************************************\
		fRemoveCallback: Remove a callback function.
	\******************************************************************************/
	core.removeCallback = function removeCallback(method, thisArg) {
		for (var i=_callbacks.length-1; i>=0; i--) {
			if (_callbacks[i].method == method) {
				if (thisArg && thisArg != _callbacks[i].thisArg) continue;
				_callbacks.splice(i,1);
			}
		}
		if (_callbacks.length == 0) {
			skapi.RemoveHandler(mtyFellowshipUpdate,  handler);
		}
	};
	
	/******************************************************************************\
		fireCallbacks: Fire a registered callback.
	\******************************************************************************/
	function fireCallbacks() {
	//	core.debug(_callbacks.length + " callbacks to fire.");
		for ( var i=_callbacks.length-1; i >= 0; i-- ) {
			_callbacks[i].method.apply(_callbacks[i].thisArg, arguments);
		}
	};

	return core;
}));